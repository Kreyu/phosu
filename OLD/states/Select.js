import AudioManager from '../system/AudioManager'
import Transition from '../system/Transition'
import Parallax from '../system/Parallax'
import Hotkey from '../system/Hotkey'

import Background from '../objects/Background'
import MapInfo from '../objects/MapInfo'
import Overlay from '../objects/Overlay'
import List from '../objects/List'
import Item from '../objects/Item'

export default class extends Phaser.State {

    init () {
        this.Background = new Background(this.game)
        this.Transition = new Transition(this.game)
        this.Parallax = new Parallax(this.game)
        this.MapInfo = new MapInfo(this.game)
        this.Overlay = new Overlay(this.game)
    }

    create () {
        this.Transition.fadeIn(350)

        this.game.audio.fadeIn(2500, this.game.userdata.sound.volume.master / 100)
        this.game.audio.manager.stop().play().preview().loop(true)

        this.selectedBeatmap = this.game.audio.manager.beatmap

        // Create stuff
        this.List = new List(this.game)
        this.Background.create('map')
        this.Overlay.create()
        this.MapInfo.create()

        // Feed list with items (hungry!)
        for (let setData of this.game.beatmaps) {
            let beatmap = {
                id: setData.folder,
                set: new Item(this.game, setData, 0, 0, 'set'),
                diffs: []
            }

            // Sort diffs by OD
            setData.difficulties.sort((a, b) => {
                a = a.Difficulty.OverallDifficulty
                b = b.Difficulty.OverallDifficulty
                if (a < b) return -1
                if (a > b) return 1
                return 0
            })

            // Create diffs
            for (let beatmapData of setData.difficulties) {
                let diff = new Item(this.game, beatmapData, 0, 0, 'beatmap')
                beatmap.diffs.push(diff)
            }

            this.List.add(beatmap)

            // Click on set events
            beatmap.set.on('click', set => {
                this.List.select(set.beatmap)
                this.Background.load(set.beatmap)
                this.MapInfo.load(this.List.getSelectedBeatmap())
                this.game.audio.manager.stop().load(set.beatmap).play().preview().loop(true)
            })

            // Click on diff events
            beatmap.diffs.forEach(difficulty => {
                difficulty.on('click', diff => {
                    if (difficulty.selected) {
                        this.Transition.fadeTo('GameState', 250, {
                            set: beatmap.set._beatmap,
                            diff: diff._beatmap
                        })
                    } else {
                        this.List.selectDiff(diff)
                        this.MapInfo.load(diff.beatmap)
                    }
                })
            })
        }

        // Select and load data of actual song
        this.List.select(this.selectedBeatmap)
        this.Background.load(this.selectedBeatmap)
        this.MapInfo.load(this.List.getSelectedBeatmap())

        // Parallax thingies
        this.Parallax.add(this.Background, 'center', 1)
        this.Parallax.run()

        // Return to menu
        new Hotkey(this.game, 'ESC', () => {
            this.Transition.fadeTo('MenuState', 250)
        })
    }

    paused () {
        this.game.audio.pause()
    }

    resumed () {
        this.game.audio.play()
    }
}
