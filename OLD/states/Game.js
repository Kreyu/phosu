import LayerManager from '../system/LayerManager'
import Path from '../system/Path'

import Transition from '../system/Transition'

import HitCircle from '../objects/HitCircle'
import Spawner from '../system/Spawner'

class GameState extends Phaser.State {

    init (mapdata) {
        this.mapdata = mapdata

        this.Transition = new Transition(this.game)
        this.LayerManager = new LayerManager(this.game)

        this.LayerManager.create(100)
    }

    create () {
        this.Transition.fadeIn(800)

		this.game.audio.manager.load(this.mapdata.set).play()
        this.game.audio.volume = this.game.userdata.sound.volume.master / 100
        this.Spawner = new Spawner(this.game, this.mapdata.diff)
    }

    update () {
        this.Spawner.loop()
    }

	paused () {
		this.game.audio.pause()
	}

	resumed () {
		this.game.audio.play()
	}    
}

export default GameState