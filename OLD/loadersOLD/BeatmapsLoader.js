import Path from '../system/Path'
import Parser from '../system/Parser'
const fs = require('fs')

export default class {

	constructor () {
		this.progress = 0
		this.isReady = false

		this.folders = null
		this.beatmaps = null

		this.allowedFormats = ['png', 'jpg', 'mp3', 'wav', 'avi', 'flv']
	}

	async run (parser) {
		this.folders = await fs.readdirSync(Path.songs, 'utf8')
		this.progress = 100 / (this.folders.length + 1)
		this.beatmaps = []
		
		for (let [index, folder] of this.folders.entries()) {
			let difficulties = await fs.readdirSync(Path.songs + '\\' + folder, 'utf8')
				.filter(filename => filename.split('.').pop() === 'osu')

			let resources = await fs.readdirSync(Path.songs + '\\' + folder, 'utf8')
				.filter(filename => this.allowedFormats.includes(filename.split('.').pop()))

			for (let [index, file] of difficulties.entries()) {
				let parsedData = await Parser.parseFile(Path.songs + '\\' + folder + '\\' + file)
				difficulties[index] = parsedData
			}

			this.beatmaps.push({
				difficulties: difficulties,
				resources: resources,
				folder: folder
			})

			this.progress += 100 / (this.folders.length + 1)
		}

		this.isReady = true
	}

}
