const WebFont = require('webfontloader')

export default class {

    constructor () {
        this.isReady = false
        this.progress = 0

        this.families = [
            'Titillium Web',
            'Roboto',
            'Lato',
        ]
    }

    run () {
        WebFont.load({
            active: () => {
                this.progress = 100
                this.isReady = true
            },
            google: {
              families: this.families
            },
            timeout: 1000
        })
    }

}
