import Path from '../system/Path'
const fs = require('fs')

export default class {

	constructor() {
		this.progress = 0
		this.isReady = false

		this.settings = null
	}
	
	async run () {
		this.settings = await fs.readFileSync(Path.system + '\\' + 'settings.json', 'utf8')
		this.settings = JSON.parse(this.settings)

		this.progress = 100
		this.isReady = true
	}

}
