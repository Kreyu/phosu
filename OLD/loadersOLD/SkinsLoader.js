import Path from '../system/Path'
const fs = require('fs')

export default class {

	constructor() {
		this.progress = 0
		this.isReady = false

		this.skins = []

		this.allowedFormats = ['png', 'jpg', 'mp3', 'wav']
	}

	async run () {
		let folders = await fs.readdirSync(Path.skins, 'utf8')

		for (let [index, folder] of folders.entries()) {
			let ini = await fs.readFileSync(Path.skins + '\\' + folder + '\\' + 'skin.ini', 'utf8')
			let resources = await fs.readdirSync(Path.skins + '\\' + folder, 'utf8')
				.filter(filename => this.allowedFormats.includes(filename.split('.').pop()))

			this.skins.push({
				name: folder,
				ini: ini,
				resources: resources
			})

			this.progress += 100 / (this.folders.length + 1)
		}
		
		this.isReady = true
	}
}
