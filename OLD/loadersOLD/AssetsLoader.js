import Path from '../system/Path'
const fs = require('fs')

export default class {

    constructor () {
        this.isReady = false
        this.progress = 0

        this.assets = {}
        this.allowedFormats = {
            image: ['png', 'jpg', 'jpeg'],
            audio: ['mp3', 'wav'],
            video: ['avi', 'flv', 'webm']
        }
    }

    async run (game) {
        let skins = game.skins
        let userdata = game.userdata
        let skinName = userdata.general.skin

        for (let skin of skins)
            if (skin.name == skinName)
                this.assets.skin = skin.resources

        this.assets.system = await fs.readdirSync(Path.assets, 'utf8')

        for (let assetPack of Object.keys(this.assets)) {
            for (let asset of this.assets[assetPack]) {
                let name = asset.replace(/\.[^/.]+$/, "")
                let ext = asset.split('.').pop()
                let path = assetPack == 'system' ?
                    `${Path.assets}\\${asset}` :
                    `${Path.skins}\\${skinName}\\${asset}`
    
                if (this.allowedFormats.image.includes(ext))
                    game.load.image(name, path)
    
                else if (this.allowedFormats.audio.includes(ext))
                    game.load.audio(name, path)
    
                else if (this.allowedFormats.video.includes(ext))
                    game.load.video(name, path)     
            }
        }

        game.load.onFileComplete.add(progress => this.progress = progress, this)
        game.load.onLoadComplete.add(() => this.isReady = true, this)
        game.load.start()
    }

}