export default class {
    constructor () {
        this.isReady = false
        this.progress = 0
    }

    run (game) {
        game.load.script('BlurX', 'https://cdn.rawgit.com/photonstorm/phaser/master/v2/filters/BlurX.js')
        game.load.script('BlurY', 'https://cdn.rawgit.com/photonstorm/phaser/master/v2/filters/BlurY.js')

        return new Promise((resolve, reject) => {
            game.load.onFileComplete.add(progress => this.progress = progress, this)
            game.load.onLoadComplete.add(() => {
                this.isReady = true
                resolve()
            }, this)
            game.load.start()
        })
    }
}
