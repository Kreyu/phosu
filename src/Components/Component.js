/**
 * @class
 * @abstract
 * @classdesc Abstract class representing a game component.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Component {

    /**
     * Component constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game 
     * @param {Layout} Layout 
     */
    constructor (game, Layout) {

        /** @private */
        this._game = game

        /** @private */
        this._layout = new Layout(game)
    }

    /**
     * Functions to run in game preload method.
     */
    preload () {
        this._layout.preload()
    }

    /**
     * Functions to run in game create method.
     */
    create () {
        this._layout.create()
        this.bindEvents()
    }

    /**
     * Functions to run in game update loop.
     */
    update () {
    }

    /**
     * Binds callbacks to layout signals.
     */
    bindEvents () {
    }

    /**
     * Returns component layout's container.
     * 
     * @returns {Phaser.Group}
     */
    getContainer () {
        return this._layout.container
    }

}