import config from './../../System/config'
import Layout from './../Layout'

/**
 * @class
 * @extends Layout
 * @classdesc Class representing a background component layout.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class extends Layout {

    /**
     * Background layout constructor
     * 
     * @constructor
     * @param {Phaser.Game} game 
     */
    constructor (game) {
        super(game)

        /** @public */
        this.onImageChange = new Phaser.Signal()
    }

    create () {
        const bgKey = config.visuals.menu.background

        this.addSprites([
            { group: 'backgrounds', imageKey: bgKey, name: 'menu' }
        ])

        this.positionize()
    }

    positionize () {
        const backgrounds = this.getGroup('backgrounds')
        const menu = backgrounds.getByName('menu')

        menu.anchor.setTo(0.5)
        menu.x = this._game.world.centerX
        menu.y = this._game.world.centerY
    }
}
