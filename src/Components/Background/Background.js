import Source from '../../System/Audio/Source'
import Layout from './Background.layout'
import Component from './../Component'

/**
 * @class
 * @extends Component
 * @classdesc Class representing a background component.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Background extends Component {
    
    /**
     * Background constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game
     */
    constructor (game) {
        super(game, Layout)
    }

}
