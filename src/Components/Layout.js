/**
 * @class
 * @abstract
 * @classdesc Abstract class representing a game component layout.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Layout {

    /**
     * Layout constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game object
     */    
    constructor (game) {

        /** @protected */
        this._game = game

        /** @private */
        this._container = game.add.group()
    }

    /**
     * Returns container group.
     * 
     * @returns {Phaser.Group}
     */
    get container () {
        return this._container
    }

    /**
     * Creates component's layout.
     */
    create () {}

    /**
     * Loads image into game cache.
     * 
     * @param {String} key Image key used to store in cache
     * @param {String} path Local image path
     */
    loadImage (key, path) {
        this._game.load.image(key, path)
    }

    /**
     * Adds sprite into game cache.
     * 
     * @param {String} group
     * @param {String} imageKey
     * @param {String} name
     */
    addSprite (group, imageKey, name) {
        const sprite = this._game.add.sprite(0, 0, imageKey)
        sprite.name = name
        
        this._addToGroup(group, sprite)
    }

    /**
     * Loads every image from given collection.
     * 
     * @param {Array} collection Array collection of images data
     */
    loadImages (collection) {
        collection.forEach(image => {
            this.loadImage(
                image.key, 
                image.path
            )
        })
    }
    
    /**
     * Adds every sprite from given collection.
     * 
     * @param {Array} collection Array collection of sprites data
     */    
    addSprites (collection) {
        collection.forEach(sprite => {
            this.addSprite(
                sprite.group,
                sprite.imageKey,
                sprite.name,
            )
        })
    }

    /**
     * Creates bitmap with given properties.
     * 
     * @param {Object} properties Object with bitmap properties
     * @returns {Phaser.BitmapData}
     */
    createBitmap (properties) {
        let bitmap = this._game.add.bitmapData(180, 10)

        bitmap.ctx.beginPath()
	    bitmap.ctx.rect(...properties.sizes)
	    bitmap.ctx.fillStyle = properties.color
        bitmap.ctx.fill()

        return bitmap
    }

    /**
     * Creates new text sprite.
     * 
     * @param {String} group
     * @param {String} name
     * @param {String} value Text string value
     * @param {Object} style Object with text styles
     */
    createText (group, name, value, style) {
        const sprite = this._game.add.text(0, 0, value, style);
        sprite.name = name

        this._addToGroup(group, sprite)
    }

    /**
     * Loops through all subgroups in main group.
     * 
     * @param {Function} callback
     */
    forEachGroup (callback) {
        const groups = this._container.children

        for (let index in groups) {
            const group = groups[index]

            callback(group, index, groups)
        }
    }

    /**
     * Loops through all sprites in container.
     * 
     * @param {Function} callback
     */
    forEachSprite (callback) {
        let index = 0

        this.forEachGroup(group => {
            group.forEach(sprite => {
                callback(sprite, index++)
            })
        })
    }

    /**
     * Gets group by name
     * 
     * @param {String} name 
     * @returns {Phaser.Group|null}
     */
    getGroup (name) {
        return this._container.getByName(name)
    }

    /**
     * Returns subgroup instance with given name.
     * 
     * @returns {Phaser.Group}
     */
    getSubgroupByName (subgroupName) {
        return this._group.getByName(subgroupName)
    }

    /**
     * Changes container's position.
     * 
     * @param {Number} posX
     * @param {Number} posY
     */
    setContainerPosition (posX, posY) {
        this._container.x = posX
        this._container.y = posY
    }

    /**
     * Changes container's anchor.
     * 
     * @param {Number} anchorX
     * @param {Number} anchorY
     */
    setContainerAnchor (anchorX, anchorY) {
        this._container.anchor.setTo(anchorX, anchorY)
    }

    /**
     * Adds sprite into group.
     * 
     * @private
     * @param {String} name
     * @param {Phaser.Sprite} sprite
     */
    _addToGroup(name, sprite) {
        this._checkGroup(name)

        this._container.getByName(name)
            .add(sprite)
    }

    /**
     * Checks if group exists.
     * If not, creates new instance and adds to container.
     * 
     * @private
     * @param {String} name
     */
    _checkGroup (name) {
        const groupExists = this._container.getByName(name)

        if (!groupExists) {
            let group = this._game.add.group()
            group.name = name

            this._container.add(group)
        }
    }

}