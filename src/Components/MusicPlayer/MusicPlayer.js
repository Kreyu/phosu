import MusicPlayerEnum from '../../System/Enum/MusicPlayerEnum';
import Source from '../../System/Audio/Source'
import Layout from './MusicPlayer.layout'
import Component from './../Component'

/**
 * @class
 * @extends Component
 * @classdesc Class representing a music player component.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class MusicPlayer extends Component {
    
    /**
     * Music player constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game
     */
    constructor (game) {
        super(game, Layout)

        /** @public */
        this.state = MusicPlayerEnum.STATE_STOPPED

        /** @private */
        this._actionPrefix = '_action'
    }

    /**
     * @inheritDoc
     */
    create () {
        super.create()

        // If audio is not playing, pick beatmap and play it.
		if (this._game.audio.isStopped()) {
			this._game.managers.beatmaps.pick()
			this._game.managers.beatmaps.load()
			this._game.audio.play()
		}

        // Update title label to current song title
        this.changeTitle(this._game.managers.beatmaps.currentTitle)        

        // Set audio volume to value set from config
        const master = this._game.managers.userdata.get('sound', 'volume', 'master') / 100
        this._game.audio.volume = master
    }

    /**
     * @inheritDoc
     */
    update () {
        this.updateProgress()

        if (this._game.audio.hasFinished()) {
            this._actionNext()
        }
    }

    /**
     * @inheritDoc
     */
    bindEvents () {
        this._layout.onProgressChange.add(this.changeProgress, this)
        this._layout.onAction.add(this._executeAction, this)
    }

    /**
     * Changes audio progress.
     * 
     * @param {Number} percentage 
     */
    changeProgress (percentage) {
        const duration = this._game.audio.duration
        const requestedTime = duration * percentage

        this._game.audio.currentTime = requestedTime
    }

    /**
     * Updates layout progress.
     */
    updateProgress () {
        const duration = this._game.audio.duration
        const currentTime = this._game.audio.currentTime
        const progress = currentTime / duration

        this._layout.setProgress(progress)
    }

    /**
     * Sets song title label's text
     * 
     * @param {String} title 
     */
    changeTitle (title) {
        this._layout.setTitle(title)
    }

    /**
     * Returns sources array
     * 
     * @returns {Array}
     */
    getSources () {
        return this._sources
    }

    /**
     * Sets sources array
     * 
     * @param {Array} sources 
     */
    setSources (sources) {
        this._sources = sources
    }

    /**
     * Adds source to array
     * 
     * @param {Source} source 
     */
    addSource (source) {
        this._sources.push(source)
    }

    /**
     * Executes given action method.
     * 
     * @private
     * @param {String} action 
     */
    _executeAction (action) {
        const method = this._actionPrefix + action.capitalize()

        this[method]()
    }

    /**
     * Play action script
     * 
     * @private
     */
    _actionPlay () {
        if (this._game.audio.isPlaying()) {
            this._game.audio.replay()
        }

        this._game.audio.play()

        this.state = MusicPlayerEnum.STATE_PLAYING
    }

    /**
     * Pause action script
     * 
     * @private
     */
    _actionPause () {
        this._game.audio.pause()

        this.state = MusicPlayerEnum.STATE_PAUSED
    }

    /**
     * Stop action script
     * 
     * @private
     */ 
    _actionStop () {
        this._game.audio.stop()

        this.state = MusicPlayerEnum.STATE_STOPPED
    }

    /**
     * Previous action script
     * 
     * @private
     */
    _actionPrevious () {
        const wasPlayingBefore = this._game.audio.isPlaying()

        if (wasPlayingBefore) {
            this._game.audio.stop()
            this.state = MusicPlayerEnum.STATE_STOPPED
        }

        this._game.managers.beatmaps.previous()
        this._game.managers.beatmaps.pickDifficulty()
        this._game.managers.beatmaps.load()

        const title = this._game.managers.beatmaps.currentTitle
        this.changeTitle(title)

        if (wasPlayingBefore) {
            this._game.audio.play()
            this.state = MusicPlayerEnum.STATE_PLAYING
        }
    }

    /**
     * Next action script
     * 
     * @private
     */
    _actionNext () {
        const wasPlayingBefore = this._game.audio.isPlaying()

        if (wasPlayingBefore) {
            this._game.audio.stop()
            this.state = MusicPlayerEnum.STATE_STOPPED
        }

        this._game.managers.beatmaps.next()
        this._game.managers.beatmaps.pickDifficulty()
        this._game.managers.beatmaps.load()

        const title = this._game.managers.beatmaps.currentTitle
        this.changeTitle(title)

        if (wasPlayingBefore) {
            this._game.audio.play()
            this.state = MusicPlayerEnum.STATE_PLAYING
        }
    }

}
