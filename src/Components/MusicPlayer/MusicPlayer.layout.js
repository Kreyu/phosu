import Layout from './../Layout'

/**
 * @class
 * @extends Layout
 * @classdesc Class representing a music player component layout.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class extends Layout {

    /**
     * Music player layout constructor
     * 
     * @constructor
     * @param {Phaser.Game} game 
     */
    constructor (game) {
        super(game)

        /** @private */
        this._buttonScale = 0.37

        /** @private */
        this._progressAlpha = 0.65

        /** @public */
        this.onProgressChange = new Phaser.Signal()

        /** @public */
        this.onAction = new Phaser.Signal()
    }

    /**
     * @inheritDoc
     */
    create () {
        this.addSprites([
            { group: 'controls', imageKey: 'controls_previous', name: 'previous' },
            { group: 'controls', imageKey: 'controls_play', name: 'play' },
            { group: 'controls', imageKey: 'controls_pause', name: 'pause' },
            { group: 'controls', imageKey: 'controls_stop', name: 'stop' },
            { group: 'controls', imageKey: 'controls_next', name: 'next' },
        ])
        
        const progressBar = this.createBitmap({
            sizes: [ 0, 0, 180, 10 ],
            color: 'rgba(255, 255, 255, 0.6)'
        })

        this.addSprites([
            { group: 'progressBar', imageKey: progressBar, name: 'background' },
            { group: 'progressBar', imageKey: progressBar, name: 'foreground' },
        ])

        this.createText('labels', 'songTitle', '', { 
            font: "22px Titillium Web", 
            fill: "#FFFFFF"
        })

        this.positionize()
        this.bindEvents()
    }

    /**
     * @inheritDoc
     */
    positionize () {
        const controls = this.getGroup('controls')
        const progressBar = this.getGroup('progressBar')
        const labels = this.getGroup('labels')

        controls.forEach((sprite, index) => {
            sprite.anchor.setTo(0.5)
            sprite.scale.setTo(this._buttonScale)
            sprite.x += index++ * 40
        })

        progressBar.forEach(sprite => {
            sprite.scale.setTo(1)
            sprite.alpha = this._progressAlpha
            sprite.x = -10
            sprite.y = 25
        })

        labels.forEach(sprite => {
            sprite.anchor.setTo(1, 0)
            sprite.x = 172
            sprite.y = -50
        })

        this.setContainerPosition(
            this._game.width - this.container.width, 70
        )
    }

    /**
     * @inheritDoc
     */
    bindEvents () {
        const controls = this.getGroup('controls')
        const progressBar = this.getGroup('progressBar')

        this.forEachSprite(sprite => {
            sprite.inputEnabled = true
        })
                
        controls.forEach(sprite => {
            sprite.events.onInputOver.add(this._onMouseOverControls, this)
            sprite.events.onInputOut.add(this._onMouseOutControls, this)
            sprite.events.onInputDown.add(this._onClickControls, this)
        })

        progressBar.forEach(sprite => {
            sprite.events.onInputOver.add(this._onMouseOverProgress, this)
            sprite.events.onInputOut.add(this._onMouseOutProgress, this)
            sprite.events.onInputDown.add(this._onClickProgress, this)
        })
    }

    /**
     * Sets progress bar's visual width.
     * 
     * @param {Number} progress 
     */
    setProgress (progress) {
        if (typeof progress !== 'number' || progress < 0 || progress > 1) {
            const message = `Progress bar requires value between 0 and 1. Given: ${progress}`
            return console.warn('[MusicPlayer] ' + message)
        }

        const progressBar = this.getGroup('progressBar')
        const background = progressBar.getByName('background')
        const foreground = progressBar.getByName('foreground')

        foreground.width = background.width * progress
    }

    /**
     * Sets song title label's text
     * 
     * @param {String} title 
     */
    setTitle (title) {
        const labels = this.getGroup('labels')
        const songTitle = labels.getByName('songTitle')

        songTitle.setText(title)
    }
    
    /**
     * Mouse over callback for controls group sprites.
     * 
     * @private
     * @param {Phaser.Sprite} sprite 
     */
    _onMouseOverControls (sprite) {
        sprite.scale.setTo(this._buttonScale + 0.1)
    }

    /**
     * Mouse out callback for controls group sprites.
     * 
     * @private
     * @param {Phaser.Sprite} sprite 
     */    
    _onMouseOutControls (sprite) {
        sprite.scale.setTo(this._buttonScale)
    }

    /**
     * Mouse over callback for progressBar group sprites.
     * 
     * @private
     */
    _onMouseOverProgress () {
        const progressBar = this.getGroup('progressBar')

        progressBar.forEach(sprite => {
            sprite.alpha = this._progressAlpha + 0.15
        })
    }

    /**
     * Mouse over callback for progressBar group sprites.
     * 
     * @private
     */
    _onMouseOutProgress () {
        const progressBar = this.getGroup('progressBar')

        progressBar.forEach(sprite => {
            sprite.alpha = this._progressAlpha
        })
    }

    /**
     * Mouse click callback for controls group sprites.
     * Dispatches onAction signal.
     * 
     * @private
     * @param {Phaser.Sprite} sprite 
     */
    _onClickControls (sprite) {
        const action = sprite.key.split('_')[1];

        this.onAction.dispatch(action)
    }

    /**
     * Mouse click callback for progressBar group sprites.
     * Dispatches onProgressChange signal.
     * 
     * @private
     */
    _onClickProgress () {
        const progressBar = this.getGroup('progressBar')
        const background = progressBar.getByName('background')
        const foreground = progressBar.getByName('foreground')

        const width = this._game.input.x - background.x - this.container.x
        const percentage = width / background.width

        foreground.width = width

        this.onProgressChange.dispatch(percentage)
    }

}
