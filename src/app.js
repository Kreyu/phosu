import UserdataManager from './System/Managers/UserdataManager';
import BeatmapManager from './System/Managers/BeatmapManager';
import UserdataLoader from './System/Loaders/UserdataLoader';
import SkinManager from './System/Managers/SkinManager';
import BootState from './States/BootState'
import MenuState from './States/MenuState'
import Audio from './System/Audio/Audio'
import globals from './System/globals'

class Game extends Phaser.Game {

    constructor() {
        super(window.innerWidth, window.innerHeight, Phaser.AUTO, 'content', null)

        this.state.add('BootState', BootState, false)
        this.state.add('MenuState', MenuState, false)

        this.audio = new Audio('music')

        this.managers = {}
        this.managers.userdata = new UserdataManager(this)
        this.managers.beatmaps = new BeatmapManager(this)
        this.managers.skins = new SkinManager(this)

        this.state.start('BootState')
    }

}

new Game()
