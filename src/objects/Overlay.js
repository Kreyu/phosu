export default class {

	// Create items in game memory

	constructor (game) {
		this.game = game

		this.author = null
		this.version = null

		this.screenFlashes = []

		this.style = {
			font: "18px Titillium Web",
			fill: "#ffffff",
			align: "center",
			stroke: 'rgba(0, 0, 0, 0.4)',
			strokeThickness: 5
		}
	}
	
	// Create sprites

	create () {
		if (this.game.state.current == 'MenuState') {
			let bgData, gradient

			bgData = this.game.add.bitmapData(this.game.world.width, 150)

			gradient = bgData.context.createLinearGradient(this.game.world.centerX * 1.4, 150, this.game.world.width, 0)
			gradient.addColorStop(0, 'rgba(0, 0, 0, 0)')
			gradient.addColorStop(1, 'rgba(0, 0, 0, 0.65)')

			bgData.context.fillStyle = gradient
			bgData.context.fillRect(0, 0, this.game.world.width, 150)

			let overlay = this.game.add.sprite(0, -20, bgData)

			this.author = this.game.add.text(0, 0, "", this.style)
			this.author.alpha = 0.4

			this.version = this.game.add.text(0, 0, "", this.style)
			this.version.anchor.setTo(1)
			this.version.alpha = 0.4
		}

		else if (this.game.state.current == 'SelectState') {
			let bgData, staticDark
			
			bgData = this.game.add.bitmapData(this.game.world.width, 250)
			staticDark  = this.game.add.bitmapData(this.game.world.width, this.game.world.height)
	
			var grd = bgData.context.createLinearGradient(
				this.game.world.centerX - 200, 300, 
				0, 0
			)
			var grd2 = bgData.context.createLinearGradient(
				0, 0,
				this.game.world.centerX, 300
			)
	
			grd.addColorStop(0, 'rgba(0, 0, 0, 0)')
			grd.addColorStop(1, 'rgba(0, 0, 0, 0.65)')
	
			grd2.addColorStop(0, 'rgba(0, 0, 0, 0)')
			grd2.addColorStop(1, 'rgba(0, 0, 0, 0.3)')
	
			bgData.context.fillStyle = grd
			bgData.context.fillRect(0, 0, this.game.world.width, 155)
	
			staticDark.context.fillStyle = grd2
			staticDark.context.fillRect(0, 0, this.game.world.width, this.game.world.height)
	
			let overlay = this.game.add.sprite(0, 0, bgData)
			let overlay3 = this.game.add.sprite(0, 0, staticDark)
		}
	}

	// Play flash animation by tweening flash sprite alpha

	flash (fadeInDur, fadeOutDur) {
		let flash, fadeIn, fadeOut

		flash = this.game.add.sprite(0, 0, 'flash')
		flash.alpha = 0

		flash.scale.setTo(this.game.world.width / flash.width)

		if (flash.height < this.game.world.height)
			flash.scale.setTo(this.game.world.height / flash.height)

		fadeIn = this.game.add.tween(flash).to({ alpha: .125 }, fadeInDur, "Linear", true)
		fadeOut = this.game.add.tween(flash).to({ alpha: 0 }, fadeOutDur, "Linear")

		fadeIn.onComplete.add(() => {
			fadeOut.start()
		})
		
		fadeOut.onComplete.add(() => {
			flash.destroy()
		})
	}

	// Set author sprite text

	setAuthor (text) {
		this.author.text = text
		this.author.x = 15
		this.author.y = this.game.world.height - 40
	}

	// Set version sprite text

	setVersion (text) {
	    this.version.text = text
		this.version.x = this.game.world.width - 15
		this.version.y = this.game.world.height - 7
	}

}
