export default class {

    constructor (game) {
        this.game = game
        this.items = []
        this.diffs = []
        this.currentIndex = null

        this.visibleItems = []

        this.addScrollHandlers()
    }

    add (item) {
        this.items.push(item)
    }

    select (beatmap) {
        this.visibleItems = []
        this.currentIndex = this.items.findIndex(item => item.id === beatmap.folder)

        this.addDiffsToList()
        this.centerActiveItem()
    }

    selectDiff (diff) {
        let newIndex

        this.visibleItems.forEach((item, index) => {
            if (item.type == 'beatmap' && item.beatmap.Metadata.Version === diff.beatmap.Metadata.Version)
                newIndex = index
        }, this)

        this.currentIndex = newIndex
        this.centerActiveItem()
    }

    addDiffsToList () {

        // Split items
        let current = this.items[this.currentIndex]
        let before = this.items.slice(0, this.currentIndex)
        let after = this.items.slice(this.currentIndex + 1, this.items.length)

        // Populate visibleItems array
        before.forEach(item => {
            this.visibleItems.push(item.set)
            item.set.visible = true
            item.diffs.forEach(diff => diff.visible = false)
        })

        current.set.visible = false
        current.diffs.forEach(diff => diff.visible = true)
        this.currentIndex = this.visibleItems.length

        this.visibleItems.push(...current.diffs)

        after.forEach(item => {
            this.visibleItems.push(item.set)
            item.set.visible = true
            item.diffs.forEach(diff => diff.visible = false)
        })
    }

    centerActiveItem () {

        // Split items
        let current = this.visibleItems[this.currentIndex]
        let before = this.visibleItems.slice(0, this.currentIndex)
        let after = this.visibleItems.slice(this.currentIndex + 1, this.visibleItems.length)

        // Helpers
        let centerX = this.game.world.centerX
        let centerY = this.game.world.centerY - current.height / 2

        before.reverse()

        this.visibleItems.forEach(item => item.x = this.game.world.width - item.width / 1.4)

        before.forEach((item, index) => {
            this.game.add.tween(item).to({ y: centerY - 50 - (85 * (index + 1)) }, 100, 'Linear', true)

            if (item.type == 'beatmap')
                item.selected = false
        })

        this.game.add.tween(current).to({ y: centerY }, 100, 'Linear', true)
        current.x -= 75
        current.selected = true

        after.forEach((item, index) => {
            this.game.add.tween(item).to({ y: centerY + 50 + (85 * (index + 1)) }, 100, 'Linear', true)
            
            if (item.type == 'beatmap')
                item.selected = false
        })        
    }

    addScrollHandlers () {
        this.game.input.mouse.mouseWheelCallback = (event) => {
            let direction = (event.deltaY < 0) ? 1 : -1
            
            this.visibleItems.forEach(item => {
                Object.values(item.sprites).forEach(sprite => {
                    sprite.y += direction * 32
                })
            })
        }
    }

    getSelectedSet () {
        return this.visibleItems[this.currentIndex]
    }

    getSelectedBeatmap () {
        return this.visibleItems[this.currentIndex].beatmap
    }
}
