export default class {

    constructor (game) {
        this.game = game
        this.sprites = []
        this.style = {
            font: '24px Titillium Web',
            fill: '#FFFFFF'
        }
    }

    create () {
        // Arist + Title + [Difficulty]
        this.title = this.game.add.text(18, 10, '', this.style)
        this.title.style.font = '28px Titillium Web'
        this.title.setShadow(0, 0, 'rgba(0,0,0,0.6)', 5)

        // Mapped by ...
        this.creator = this.game.add.text(18, 43, '', this.style)
        this.creator.style.font = '19px Titillium Web'
        this.creator.setShadow(0, 0, 'rgba(0,0,0,0.5)', 4)
        
        // Length + BPM + Objects
        this.length = this.game.add.text(18, 72, '', this.style)
        this.length.style.font = '18px Titillium Web'
        this.length.setShadow(0, 0, 'rgba(0,0,0,0.4)', 4)

        // Circles + Sliders + Spinners
        this.hitobjects = this.game.add.text(18, 93, '', this.style)
        this.hitobjects.style.font = '18px Titillium Web'
        this.hitobjects.setShadow(0, 0, 'rgba(0,0,0,0.4)', 4)

        // CS + AR + AT + HP + Stars
        this.difficulty = this.game.add.text(18, 121, '', this.style)
        this.difficulty.style.font = '15px Titillium Web'
        this.difficulty.setShadow(0, 0, 'rgba(0,0,0,0.3)', 3)
        
        // All sprites
        this.sprites = [ 
            this.title, 
            this.creator, 
            this.length, 
            this.hitobjects,
            this.difficulty
        ]
    }

    load (beatmap) {
        let objectQuantity = beatmap.Counters.circle + beatmap.Counters.slider + beatmap.Counters.spinner
        let duration = parseInt(beatmap.HitObjects.pop().time) / 1000
        let min = Math.floor(duration / 60)
        let sec = Math.floor(duration % 60).toString().padStart(2, '0')

        this.title.text = `${beatmap.Metadata.Artist} - ${beatmap.Metadata.Title} [${beatmap.Metadata.Version}]`
        this.creator.text = `Mapped by ${beatmap.Metadata.Creator}`
        this.length.text = `Length: ${min}:${sec}  Objects: ${objectQuantity}`
        this.hitobjects.text = `Circles: ${beatmap.Counters.circle}  Sliders: ${beatmap.Counters.slider}  Spinners: ${beatmap.Counters.spinner}`
        this.difficulty.text = `AR ${beatmap.Difficulty.ApproachRate}  CS ${beatmap.Difficulty.CircleSize}  HP ${beatmap.Difficulty.HPDrainRate}  OD ${beatmap.Difficulty.OverallDifficulty}`
    }
    
}
