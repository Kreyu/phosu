export default class {

	// Create items in game memory

	constructor (game, properties) {

		this.game		= game
		this.colour 	= properties.colour || 'rgba(0, 0, 0, 0.4)'
		this.duration 	= properties.duration || 2000
		this.fontSize 	= properties.fontSize || 24
		this.height 	= properties.height || 100
		this.text 		= properties.text || ''
		
		this.isVisible 	= false

		this.sprites 	= game.add.group()
		this.message 	= game.add.text(0, 0)
		this.background = game.add.sprite(0, 0)

		this.createBackground()
		this.createMessage()
	}

	// Create background sprite and add it to sprites group

	createBackground () {
		let data = this.game.add.bitmapData(this.game.world.width, this.height)
		data.ctx.beginPath()
	    data.ctx.rect(0, 0, this.game.world.width, this.height)
	    data.ctx.fillStyle = this.colour
	    data.ctx.fill()

        this.background.loadTexture(data, true)
	    this.background.anchor.setTo(.5)
	    this.background.x = this.background.width / 2
	    this.background.y = this.game.world.centerY
		this.background.height = 0
		this.background.alpha = 0

		this.sprites.add(this.background)
	}

	// Create message text sprite and add it to sprites group

	createMessage () {
		this.message.text = this.text
		this.message.cssFont = this.fontSize + "px Titillium Web"
		this.message.fill = "#ffffff"
		this.message.align = "center"
		
		this.message.setShadow(0, 0, 'rgba(0, 0, 0, 0.9)', 8)
		this.message.anchor.setTo(.5)
		this.message.alpha = 0

	    this.message.x = this.background.width / 2
		this.message.y = this.game.world.centerY
		
		this.sprites.add(this.message)
	}

	// Update message text sprite

	updateMessage () {
		this.message.text = this.text
	}

	// Show alert by tweening its alpha and height in 500ms, or in duration provided by user

	show (duration = 500) {
		const tweens = {
			fadeIn: this.game.add.tween(this.background).to({ alpha: 1 }, duration, "Linear", true),
			openIn: this.game.add.tween(this.background).to({ height: this.height }, 200, "Linear", true),
			fadeInMessage: this.game.add.tween(this.message).to({ alpha: 1 }, duration, "Linear", true)
		}

		tweens.fadeIn.onComplete.add(() => {
			this.game.time.events.add(this.duration, this.hide, this)
		}, this)

		this.isVisible = true
	}

	// Hide alert by tweening its alpha and height in 500ms, or in duration provided by user, then run destroy method

	hide (duration = 500) {
		const tweens = {
			fadeOut: this.game.add.tween(this.background).to({ alpha: 0 }, duration, "Linear", true),
			fadeOutMessage: this.game.add.tween(this.message).to({ alpha: 0 }, duration, "Linear", true)
		}

		tweens.fadeOut.onComplete.add(this.destroy, this)

		this.isVisible = false
	}

	// Destroy every sprites group child

	destroy () {
        this.sprites.removeAll(true)
	}
}
