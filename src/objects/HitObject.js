export default class {

    constructor (game, x, y, time, type, hitSound) {
        this.game = game
        this.x = x
        this.y = y
        this.time = time
        this.type = type
        this.hitSound = hitSound
    }

}