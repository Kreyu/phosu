// @ts-check

import SoundFX from '../system/SoundFX'
import HitObject from './HitObject'

export default class extends HitObject {

    constructor (game, x, y, time, type, hitSound) {
        super(game, x, y, time, type, hitSound)

        this.circle = game.add.sprite(x, y)
        this.outline = game.add.sprite(x, y)
        this.sprites = game.add.group()

        this.combo = 1

        this.create()
        this.approach()
    }

    create () {
        this.circle.loadTexture('hitcircle')
        this.outline.loadTexture('approachcircle')

        this.circle.anchor.setTo(.5)
        this.outline.anchor.setTo(.5)
        this.outline.scale.setTo(2)

        this.sprites.add(this.circle)
        this.sprites.add(this.outline)
    }

    approach () {      
        let animation = this.game.add.tween(this.outline.scale).to({
            x: 1, y: 1
        }, this.time, "Linear", true)

        animation.onComplete.add(() => this.destroy())
    }

    destroy () {
        let sound = new SoundFX(this.game)
        sound.play('drum-hitnormal', this.game.userdata.sound.volume.master / 50)
        this.sprites.removeAll(true)
    }

}