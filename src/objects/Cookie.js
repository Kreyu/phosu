export default class {

	// Create items in game memory

	constructor (game, scale) {
		this.game = game

		this.scaleModifier = scale || 1

		this.isClickable = true
		this.isClosing = false
		this.isBeat = false

        this.sprites = game.add.group()
        this.logo = game.add.sprite(0, 0)
        this.ghost = game.add.sprite(0, 0)
        this.ring = game.add.sprite(0, 0)

        this.onHover = {
            tween: null,
            value: 0 
		}
	}

	// Create sprites and apply properties

	create (x = this.game.world.centerX, y = this.game.world.centerY) {
		let graphics = this.game.add.graphics(0, 0)
		graphics.lineStyle(10, 0xffffff)
		graphics.drawCircle(0, 0, 650)

		this.ring.addChild(graphics)
		this.ring.x = x
		this.ring.y = y

		this.logo.loadTexture('logo', true)
		this.logo.x = x
		this.logo.y = y

		this.ghost.loadTexture('logo', true)
		this.ghost.x = x
		this.ghost.y = y

		this.sprites.add(this.logo)
		this.sprites.add(this.ghost)
		this.sprites.add(this.ring)

		// Properties
		this.game.physics.arcade.enable(this.sprites)
		
		this.sprites.forEach(sprite => {
			sprite.anchor.setTo(.5)
			sprite.scale.setTo(1 * this.scaleModifier)
			sprite.body.setCircle(sprite.width / 2)
			sprite.inputEnabled = true
			sprite.events.onInputOver.add(() => this.hover(true), this)
			sprite.events.onInputOut.add(() => this.hover(false), this)
		}, this)		

		this.ghost.alpha = 0
		this.ring.alpha = 0
		this.ring.anchor.setTo(.5)
		this.ghost.inputEnabled = true		
	}

	// Create & animate beat ring

	createRing (interval, power) {
		power /= 1000

		this.ring.x = this.logo.x
		this.ring.y = this.logo.y
		this.ring.scale.setTo(this.logo.scale.x + .2)
		this.ring.alpha = .1

		this.game.add.tween(this.ring.scale).to({ 
			x: .8 + power, 
			y: .8 + power 
		}, interval, Phaser.Easing.Sinusoidal.Out, true)

		this.game.add.tween(this.ring).to({
			alpha: 0
		}, interval, "Linear", true)
	}

	// Change onHover value property on hover/mouseOver by tweening it

    hover (isOver) {
		this.onHover.tween = isOver ?
			this.game.add.tween(this.onHover).to({ value: 0.05 }, 200, "Linear") :
			this.game.add.tween(this.onHover).to({ value: 0.00 }, 200, "Linear")

		this.onHover.tween.start()
    }	

	// Play pulse animation

	pulse () {
		if (!this.isBeat) {
			let scale = (.5 - this.game.audio.average / 2000 + this.onHover.value) * this.scaleModifier
			this.sprites.forEach(sprite => {
				if (sprite.children.length == 0)
					sprite.scale.setTo(scale)
			}, this)
		}
	}

	// Play beat animation

    beat () {
		this.isBeat = true
		if (!this.isClosing) {
			this.sprites.forEach(sprite => {
				if (sprite.children.length == 0) {
					let scaleIn = this.game.add.tween(sprite.scale).to({ x: '+0.05', y: '+0.05' }, 50, "Linear", true)
					let scaleOut = this.game.add.tween(sprite.scale).to({ x: '-0.05', y: '-0.05' }, 150, "Linear")

					scaleIn.onComplete.add(() => scaleOut.start())
					scaleOut.onComplete.add(() => this.isBeat = false)
				}
			}, this)
		}
	}
	
	// Play close animation (e.g. when leaving menu state)

	close () {
		this.isClosing = true
		this.sprites.forEach(sprite => {
			let scaleOut = this.game.add.tween(sprite.scale).to({ x: '-0.25', y: '-0.25' }, 1500, Phaser.Easing.Sinusoidal.Out, true)
			let rotate = this.game.add.tween(sprite).to({ angle: '-8' }, 1500, Phaser.Easing.Sinusoidal.Out, true)
		}, this)
	}

	// On event callback

	on (event, cb) {
		const events = {
			click: () => this.click(cb)
		}

		events[event]()
	}

	// On click event, invoked by on() method

	click (callback) {
		this.ghost.events.onInputDown.add(() => {
			if (this.isClickable) {
				this.isClickable = false
				callback()
			}
		}, this)
	}
}
