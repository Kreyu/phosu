export default class {
   
    // Create items in game memory

    constructor (game) {
        this.game = game

        this.bar = {
            alpha: 0.6,
            strength: 8,

            size: {
                x: 8, 
                y: 10 
            }
        }

        this.loop = null
        this.counter = 0
        this.rotationSpeed = 0.1

        this.sprites = game.add.group()
    }

    // Create bars from bitmapData

    create () {
        let data = this.game.add.bitmapData(this.bar.size.x, this.bar.size.y),
        gradient = data.context.createLinearGradient(this.bar.size.x / 2, 0, this.bar.size.x / 2, this.bar.size.y / 4)
		gradient.addColorStop(0, 'rgba(255, 255, 255, 1)')
		gradient.addColorStop(1, 'rgba(255, 255, 255, .1)')

        data.ctx.fillStyle = gradient
		data.ctx.fillRect(0, 0, this.bar.size.x, this.bar.size.y)

        for (let index = 0; index <= this.game.audio.freq.length; index++) {
            let sprite = this.game.add.sprite(0, 0, data)
            sprite.alpha = this.bar.alpha

            this.sprites.add(sprite)
        }

        this.loop = this.game.time.events.loop(20, () => {
            this.counter += this.rotationSpeed
        })
    }

    update (logo) {
        let startFreq = 50,
            splitFreq = this.game.audio.freq.slice(startFreq, startFreq + this.game.audio.freq.length/2),
            frequency = [...splitFreq, ...splitFreq]

        for (let index = 0; index < this.sprites.children.length; index++) {
            let sprite, overlay, angle, position

            sprite = this.sprites.children[index]
            overlay = new Phaser.Circle(this.game.world.centerX, this.game.world.centerY, logo.width - 35)
            if (index % 3 == 0) angle = index  + this.counter
            position = overlay.circumferencePoint(angle, true)
            
            sprite.angle = angle - 90
            sprite.height = frequency[index] * this.bar.strength * (this.game.world.width * .00009)
            sprite.x = - (this.game.world.centerX - logo.x) + position.x
            sprite.y = - (this.game.world.centerY - logo.y) + position.y
        }
        
        let sum = 0
        for (let index = 0; index < frequency.length; index++)
            sum += frequency[index]
        let average = sum / frequency.length

        // console.log()
        this.rotationSpeed = average * .003

    }
}
