import Alert from './Alert'

export default class {

	constructor (game, updateInterval = 300) {
		this.game = game
		this.updateInterval = updateInterval

		this.progressUpdater = null
		this.soundEffect = null
		this.metadata = null
		this.alerts = []

		this.sprites = {
			title: null,
			progress: {
				value: null,
				bar: null
			},
			controls: {
				next: null,
				stop: null,
				pause: null,
				play: null,
				previous: null
			}
		}

	}

	//

	create () {
		this.createTitle()
		this.createControls()
		this.createProgress()

		this.positionSprites()

		this.addHoverHandlers()
		this.addInputHandlers()

		this.updateMetadata()
		this.updateTitle()
		this.setUpdateProgressInterval()
	}

	//

	createTitle () {
		let title = this.game.add.text(0, 0, '', {
			font: "48px Titillium Web", 
			fill: "#ffffff", 
			align: "right"
		})

		title.setShadow(0, 0, 'rgba(0, 0, 0, 0.75)', 8)
		title.scale.setTo(.7)
		this.sprites.title = title
	}

	createControls () {
		let controls = {
			next: this.game.add.sprite(0, 0, 'next'),
			stop: this.game.add.sprite(0, 0, 'stop'),
			pause: this.game.add.sprite(0, 0, 'pause'),
			play: this.game.add.sprite(0, 0, 'play'),
			previous: this.game.add.sprite(0, 0, 'previous')
		}

		this.sprites.controls = controls
	}

	createProgress () {
		let bitmap = this.game.add.bitmapData(180, 10)
		bitmap.ctx.beginPath()
	    bitmap.ctx.rect(0, 0, 180, 10)
	    bitmap.ctx.fillStyle = 'rgba(255, 255, 255, 0.6)'
		bitmap.ctx.fill()

		let progress = {
			bar: this.game.add.sprite(0, 0, bitmap),
			value: this.game.add.sprite(0, 0, bitmap)
		}

		progress.bar.anchor.setTo(1)
		progress.value.width = 0
		progress.bar.alpha = 0.25
		progress.value.alpha = 0.8		

		this.sprites.progress = progress
	}

	positionSprites () {
		let title = this.sprites.title
		let progress = this.sprites.progress
		let controls = Object.values(this.sprites.controls)

		let x = 0
		let y = 0

		title.scale.setTo(.5)
		title.anchor.setTo(.5)

		title.x = this.game.world.width - title.width / 2 - this.sprites.controls.play.width / 2
		title.y = 32

	    progress.bar.x = this.game.world.width - 25
	    progress.bar.y = 105
	    progress.value.x = progress.bar.x - progress.bar.width
	    progress.value.y = 105 - progress.bar.height

		controls.forEach((sprite, index) => {
            sprite.anchor.setTo(.5)
			sprite.scale.setTo(.35)
			sprite.x = this.game.world.width - sprite.width - 40 * index
			sprite.y = 65
		})
	}

	addHoverHandlers () {
		let progress = Object.entries(this.sprites.progress)
		let controls = Object.values(this.sprites.controls)

		controls.forEach(sprite => {
			sprite.inputEnabled = true
            sprite.events.onInputOver.add(() => {
				sprite.scale.setTo(.4)
				this.playSound()
            }, this)
            sprite.events.onInputOut.add(() => {
            	sprite.scale.setTo(.35)
            }, this) 			
		})

		progress.forEach(([key, sprite]) => {
			sprite.inputEnabled = true

			sprite.events.onInputOver.add(() => {
				progress[0][1].alpha = 0.45
				progress[1][1].alpha = 0.95
			})

			sprite.events.onInputOut.add(() => {
				progress[0][1].alpha = 0.25
				progress[1][1].alpha = 0.80				
			})
		})	
	}

	addInputHandlers () {
		let progress = Object.values(this.sprites.progress)
		let controls = Object.values(this.sprites.controls)

		controls.forEach(sprite => {
			sprite.inputEnabled = true
			sprite.events.onInputDown.add(() => {
				this.game.audio.manager[sprite.key]()

				this.updateMetadata()
				this.updateTitle()

				if (this.alerts.length > 0)
					this.alerts.forEach(alert => alert.hide(100))

				let alert = new Alert(this.game, {
					text: sprite.key[0].toUpperCase() + sprite.key.slice(1),
					fontSize: 32
				})
				
				this.alerts.push(alert)
				alert.show()
			}, this)
		})

		progress.forEach(sprite => {
			sprite.events.onInputDown.add(() => {
				let area = this.sprites.progress.value.x + this.sprites.progress.bar.width
				let clickPos = (area - this.game.input.mousePointer.x) / this.sprites.progress.bar.width
				let clickedProgress = 1 - clickPos
				let tick = this.game.audio.duration / 100
		
				this.game.audio.progress = clickedProgress * 100 * tick
				this.updateProgress()
			})
		})
	}

	updateMetadata () {
		this.metadata = this.game.audio.manager.beatmap.difficulties[0].Metadata
	}

	updateTitle () {
		let old_title = this.sprites.title.text
		let new_title = this.metadata.Artist + ' - ' + this.metadata.Title

		if (old_title !== new_title) {
			this.sprites.title.text = new_title
			this.sprites.title.x = this.game.world.width - this.sprites.title.width / 2 - this.sprites.controls.play.width / 2
		}
	}

	setUpdateProgressInterval () {
		this.progressUpdater = setInterval(() => this.updateProgress(), this.updateInterval, this)
	}

	updateProgress () {
		let duration = this.game.audio.duration
		let progress = this.game.audio.progress
		let tick = this.sprites.progress.bar.width / 100

		if (this.game.audio.isPlaying)
			this.sprites.progress.value.width = tick * (progress / duration * 100)
	}

	playSound () {
		this.soundEffect = this.game.add.audio('menuclick')
		this.soundEffect.volume = this.game.userdata.sound.volume.master / 50
		this.soundEffect.play()		
	}
}
