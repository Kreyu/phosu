import config from '../System/config'
const Path = config.path

export default class {

    // Create items in game memory

    constructor (game) {
        this.game = game

        this.key = null
        this.prefix = null
        this.sprites = game.add.group()
        this.background = game.add.sprite(0, 0)
    }

    // Create sprite, set its properties and add to sprites group

    create () {
        this.key =
            this.game.state.current == 'MenuState' ?
            'background' : 'map-background'

        this.background.x = this.game.world.centerX
        this.background.y = this.game.world.centerY
        this.background.anchor.setTo(.5)
        this.background.alpha = .9

        if (this.key == 'background')
            this.reload()

        this.sprites.add(this.background)
    }

    // Load file into Phaser's cache, then run reload method

    load (beatmap) {
        let folder = beatmap.folder
        let image = beatmap.difficulties[0].Metadata.Background

        if (Array.isArray(image))
            image = image[0]

        this.game.load.image(this.key, Path.songs + '\\' + folder + '\\' + image)
        this.game.load.onLoadComplete.add(() => this.reload(), this)
        this.game.load.start()
    }

    // Reload background sprite to apply new texture

    reload () {
        try {
            this.background.loadTexture(this.key, true)
        } catch (e) {
            this.destroy()
            this.create() 
        }

        this.scale()
    }

    // Scale background sprite to fit the screen

    scale (offset = 0.05) {
        this.background.scale.setTo(1)

        while (this.background.width < this.game.world.width) {
            this.background.scale.x += 0.01
            this.background.scale.y += 0.01
        }

        this.background.scale.x += offset
        this.background.scale.y += offset
    }

    destroy () {
        this.background.destroy()
        this.sprites.removeAll(true)
    }

}