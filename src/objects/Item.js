export default class {

    constructor (game, beatmap, x, y, type) {
        this.game = game

        this._beatmap = beatmap
        this._x = x
        this._y = y
        this._type = type || 'set'

        this._width = null
        this._height = null

        this._visible = true
        this._played = true
        this._selected = false

        this.sprites = []

        this.create()
    }

    //
    
    get x () { return this._x }
    get y () { return this._y }
    get type () { return this._type }
    get visible () { return this._visible }
    get played () { return this._played }
    get selected () { return this._selected }
    get width () { return this._width }
    get height () { return this._height }
    get beatmap () { return this._beatmap }

    set x (x) {
        this._x = x
        this.setPosition(x, null)
    }

    set y (y) {
        this._y = y
        this.setPosition(null, y)
    }

    set type (type) {
        this._type = type
        this.colorize()
    }

    set visible (visible) {
        this._visible = visible
        visible ? this.show() : this.hide()
    }

    set played (played) {
        this._played = played 
        this.colorize()
    }

    set selected (selected) {
        this._selected = selected
        this.colorize()
    }

    //

    create () {
        this.createBackground()
        this.createTitle()
        this.createArtist()

        if (this._type == 'beatmap') {
            this.createDifficulty()
            this.createStars()
        }

        this.createOverlay()
        this.colorize()

        this.addHoverHandlers()
        this.addInputHandlers()
    }

    createBackground () {
        let background = this.game.add.sprite(0, 0, 'song_select_background')
        background.scale.setTo(.85)

        background.alpha = 0.75

        this._width = background.width - 10
        this._height = background.height - 10

        background.x = this._x
        background.y = this._y

        this.background = background
        this.sprites.push(background)
    }

    createTitle () {
        let title = this.game.add.text(0, 0, '', {
            font: "30px Titillium Web",
            fill: "#ffffff",
            align: "left"
        })

        title.scale.setTo(.8)
        title.text = this._type == 'set' ? 
            this._beatmap.difficulties[0].Metadata.Title :
            this._beatmap.Metadata.Title
        title.setShadow(0, 0, 'rgba(0, 0, 0, 0.2)', 8)
        
        title.x = this.background.x + 70
        title.y = this.background.y + 16

        this.title = title
        this.sprites.push(title)
    }

    createArtist () {
        let artist = this.game.add.text(0, 0, '', {
            font: "24px Titillium Web",
            fill: "#ffffff",
            align: "left"
        })

        artist.scale.setTo(.8)
        artist.text = this._type == 'set' ? 
            this._beatmap.difficulties[0].Metadata.Artist + ' // ' + this._beatmap.difficulties[0].Metadata.Creator :
            this._beatmap.Metadata.Artist + ' // ' + this._beatmap.Metadata.Creator
        artist.setShadow(0, 0, 'rgba(0, 0, 0, 0.2)', 8)
        
        artist.x = this.background.x + 72
        artist.y = this.background.y + 44

        this.artist = artist
        this.sprites.push(artist)
    }

    createDifficulty () {
        let difficulty = this.game.add.text(0, 0, '', {
            font: "bold 22px Titillium Web",
            fill: "#ffffff",
            align: "left"
        })

        difficulty.scale.setTo(.8)
        difficulty.text = this._beatmap.Metadata.Version
        difficulty.setShadow(0, 0, 'rgba(0, 0, 0, 0.2)', 8)
        
        difficulty.x = this.background.x + 72
        difficulty.y = this.background.y + 68

        this.difficulty = difficulty
        this.sprites.push(difficulty)
    }

    createStars () {
        let overallDifficulty = 5
        this.stars = []

        for (let i=0; i<10; i++) {
            let star = this.game.add.sprite(0, 0, 'star')
            star.x += this.background.x + 59 + (star.width / 2 * i) + 10
            star.y += this.background.y + 90
            star.scale.setTo(.6)

            if (i < this._beatmap.Difficulty.OverallDifficulty)
                star.tint = 0xd8223d

            this.stars.push(star)
            this.sprites.push(star)
        }
    }

    createOverlay () {
        let overlay = this.game.add.sprite(0, 0, 'song_select_background')
        overlay.scale.setTo(.85)
        overlay.alpha = 0

        overlay.x = this._x
        overlay.y = this._y

        this.overlay = overlay
        this.sprites.push(overlay)
    }

    addHoverHandlers (overCb, outCb) {
        this.overlay.inputEnabled = true

        this.overlay.events.onInputOver.add(() => {
            this.setPosition(this.background.x - 25, null)
            this.soundEffect = this.game.add.audio('menuclick')
            this.soundEffect.volume = this.game.userdata.sound.volume.master / 50
            this.soundEffect.play()

            if (overCb) overCb()
        }, this)

        this.overlay.events.onInputOut.add(() => {
            this.setPosition(this.background.x + 25, null)

            if (outCb) outCb()
        }, this)
    }

    addInputHandlers (cb) {
        this.overlay.inputEnabled = true

        this.overlay.events.onInputDown.add(() => {
            if (this._selected) {
                this.soundEffect = this.game.add.audio('menuhit')
                this.soundEffect.volume = this.game.userdata.sound.volume.master / 50
                this.soundEffect.play()
            }

            if (cb) cb(this)
        }, this)  
    }

    on (event, cb, cb2) {
        let events = {
            hover: () => this.addHoverHandlers(cb, cb2),
            click: () => this.addInputHandlers(cb)
        }
        events[event]()
    }

    colorize () {
        let fill, tint

        if (this._type == 'beatmap') {
            if (this._selected) {
                fill = '#d8223d'
                tint = 0xdbdbdb
            } else {
                fill = '#ffffff'
                tint = 0x5396e8 
            }
        }

        else if (this._type == 'set') {
            tint = this._played ? 0xf46b42 : 0xba327c
            fill = '#ffffff'
        }

        this.title.fill = fill
        this.artist.fill = fill

        if (this._type == 'beatmap')
            this.difficulty.fill = fill

        this.background.tint = tint
    }

    /**
     * Methods
     */

    show () {
        for (let sprite of this.sprites)
            sprite.visible = true

        this._visible = true
    }

    hide () {
        for (let sprite of this.sprites)
            sprite.visible = false

        this._visible = false
    }

    toggle () {
        this._visible ? this.hide() : this.show()
    }

    setPosition (x, y) {
        if (x !== null) {
            this.background.x = x
            this.title.x = x + 69
            this.artist.x = x + 70

            if (this._type == 'beatmap') {
                this.difficulty.x = x + 70
                for (let [index, star] of this.stars.entries())
                    star.x = x + 66 + (star.width / 1.3 * index)
            }

            this.overlay.x = x
        }

        else if (y !== null) {
            this.background.y = y
            this.title.y = y + 17
            this.artist.y = y + 43

            if (this._type == 'beatmap') {
                this.difficulty.y = y + 65
                for (let [index, star] of this.stars.entries())
                    star.y = y + 88
            }

            this.overlay.y = y
        }
    }
}
