import { app, ipcMain, BrowserWindow } from 'electron'
import path from 'path'
import url from 'url'

let splash, game,
		iconPath = path.join(__dirname, '../static/System/assets/icon.png')

console.log(iconPath)

app.on('ready', () => {

	splash = new BrowserWindow({
		width: 300,
		height: 300,
		frame: false,
		show: false,
		transparent: true,
		center: true,
		alwaysOnTop: true,
		icon: iconPath
	})

	splash.loadURL(url.format({
		pathname: path.join(__dirname, 'splash.html'),
		protocol: 'file:',
		slashes: true
	}))

	splash.once('ready-to-show', () => {
		splash.show()
	})

	game = new BrowserWindow({
		width: 1280,
		height: 720,
		show: true,
		icon: iconPath
	})

	game.loadURL(url.format({
		pathname: path.join(__dirname, 'app.html'),
		protocol: 'file:',
		slashes: true
	}))

})

ipcMain.on('loading-progress', (event, data) => {
	splash.webContents.send('progress-update', { progress: data })
})

ipcMain.on('loading-complete', () => {
	splash.hide()
	game.show()
})

app.on('window-all-closed', () => app.quit())
