/**
 * AUDIO CONTROLLER
 * Used to control main audio element using Web Audio API
 */

export default class {

    constructor(id, arraySize) {
        this.sound = document.getElementById(id)
        this.arraySize = arraySize || 128

        this.frequencyData = null
        this.audioContext = null
        this.audioSource = null
        this.audioFilter = null
        this.analyser = null

        this.fadeInterval = null

        this._average = 0
        this._max = 0

        this.createAudioContext()
    }
    
    get average () { return this._average }
    get max () { return this._max }
    get freq () { return this.frequencyData }

    get src () { return this.sound.src }
    get duration () { return this.sound.duration }
    get volume () { return this.sound.volume }
    get state () { return this.sound.readyState }
    get speed () { return this.sound.playbackRate }
    get error () { return this.sound.error }
    get progress () { return this.sound.currentTime }    

    get isPlaying () {  return !this.sound.paused  }
    get isPaused () { return this.sound.paused  }
    get isStopped () { return this.sound.paused && this.sound.currentTime == 0 }
    get isMuted () { return this.sound.volume == 0 }
    get isBuffering () { return this.sound.readyState == 3 }
    get isReady () { return this.sound.readyState == 4 }
    get isAutoplay () { return this.sound.autoplay }
    get isLooped () { return this.sound.loop }

    set volume (volume) {
        volume > 1 || volume < 0 ?
            console.warn(`Wrong volume given: ${volume} [0-1]`) :
            this.sound.volume = volume
    }

    set src (src) {
        typeof (src) !== 'string' || src === '' ?
            console.warn(`Wrong source given: ${src}  [string]`) :
            this.sound.src = src
    }

    set progress (progress) {
        this.sound.currentTime = progress
    }

    set speed (speed) {
        this.sound.playbackRate = speed
    }

    set autoplay (autoplay) {
        typeof (autoplay) !== 'boolean' ?
            console.warn(`Wrong autoplay property, given: ${autoplay} [bool]`) :
            this.sound.autoplay = autoplay
    }

    set looped (looped) {
        typeof (autoplay) !== 'boolean' ?
            console.warn(`Wrong looped property, given: ${looped} [bool]`) :
            this.sound.looped = looped        
    }

    createAudioContext () {
        this.frequencyData = new Uint8Array(this.arraySize)
        this.audioContext = new (window.AudioContext || window.webkitAudioContext)()
        this.audioSource = this.audioContext.createMediaElementSource(this.sound)
        this.audioFilter = this.audioContext.createBiquadFilter()
        this.audioSource.connect(this.audioFilter)
        this.analyser = this.audioContext.createAnalyser()
        this.audioSource.connect(this.analyser)
        this.audioSource.connect(this.audioContext.destination)
    }

    analyze () {
        this.analyser.getByteFrequencyData(this.frequencyData)
        this._average = this.frequencyData.reduce((a, b) => a + b) / this.frequencyData.length
        if (this._average > this._max) this._max = this._average
    }

    filter (type) {
        let filterType = type || 'allpass'
        
        this.audioFilter.type = filterType
        this.audioFilter.frequency.value = 1000
        this.audioFilter.Q.value = 10
        this.audioFilter.gain.value = 20
        this.audioFilter.connect(this.audioContext.destination)
        return this
    }

    load () {
        this.sound.load()
        return this
    }

    play () {
        if (!this.sound.paused) {
            this.sound.pause()
            this.sound.currentTime = 0            
        }

        this.sound.play()
        return this
    }

    pause () {
        this.sound.pause()
        return this
    }

    stop () {
        this.sound.pause()
        this.sound.currentTime = 0
        return this
    }

    hideControls () {
        this.sound.controls = false
        return this
    }

    showControls () {
        this.sound.controls = true
        return this
    }

    async fade (outDuration = 1000, inDuration = 1000, action) {
        await this.fadeOut(outDuration)
        if (action !== undefined) action()
        await this.fadeIn(inDuration)
    }

    fadeIn (duration = 1000, toVal = 1) {
        let startVolume = 0
        let interval = duration / (toVal * 100)

        return new Promise((resolve, reject) => {
            this.fadeInterval = setInterval(() => {
                if (this.sound.volume > (toVal - 0.01)) {
                    this.sound.volume = toVal
                    clearInterval(this.fadeInterval)
                    resolve()
                } else
                    this.sound.volume += 0.01
            }, interval, this)
        })
    }

    fadeOut (duration = 1000) {
        let startVolume = this.sound.volume * 100
        let interval = duration / startVolume

        return new Promise((resolve, reject) => {
            this.fadeInterval = setInterval(() => {
                if (this.sound.volume < 0.01) {
                    this.sound.volume = 0
                    clearInterval(this.fadeInterval)
                    resolve()
                } else
                    this.sound.volume -= 0.01
            }, interval, this)
        })
    }

    on (event, cb) {
        let events = {
            'ready': () => this.ready(cb),
            'end': () => this.end(cb)
        }
        events[event]()
    }

    ready(cb) {
        this.readyState = setInterval(() => {
            if (this.sound.readyState == 4) {
                cb()
                clearInterval(this.readyState)
            }
        }, 10)     
    }

    end(cb) {
        this.sound.addEventListener("ended", () => cb(this), false)
    }
}
