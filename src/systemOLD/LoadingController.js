import { ipcRenderer } from 'electron'

export default class {

    constructor () {
        this._progress = 0
    }

    set progress (progress) {
        if (progress !== this._progress) {
            this._progress = progress
            ipcRenderer.send('loading-progress', progress)
        }
    }

    complete () {
        ipcRenderer.send('loading-complete')
    }

}
