export default class {

	constructor (game) {
		this.game = game
		this.sprites = []
	}

	add (object, position, intensity) {
		let sprites = object.sprites
		
		if (position == 'center')
			position = {
				x: this.game.world.centerX,
				y: this.game.world.centerY
			}

		sprites.forEach(sprite => {
			this.sprites.push({
				sprite: sprite,
				position: position,
				intensity: intensity * 100
			})
		}, this)
		
		this.update()
	}

	update () {
		for (let sprite of this.sprites) {
			sprite.sprite.x = sprite.position.x + (this.game.world.centerX - this.game.input.mousePointer.x) / sprite.intensity
			sprite.sprite.y = sprite.position.y + (this.game.world.centerY - this.game.input.mousePointer.y) / sprite.intensity			
		}
	}

	run () {
        this.game.input.addMoveCallback((pointer, x, y) => {
            this.update()
        })		
	}

}
