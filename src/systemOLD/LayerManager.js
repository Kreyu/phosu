/**
 |---------------------------------------------------
 | Layer manager
 |---------------------------------------------------
 |
 | This file holds layer manager class.
 | Built to give app some flexibility in terms of creating sprites
 |
 */

export default class {

    constructor (game) {
        this.game = game

        this.layers = []
        this.quantity = null
    }

    create (quantity) {
        this.quantity = quantity

        for (let i=0; i<=quantity; i++) {
            this.layers[i] = this.game.add.group()
        }
    }

    addSprite (sprite, layerIndex) {
        this.layers[layerIndex].add(sprite)
    }

}