/**
 |---------------------------------------------------
 | Hotkey
 |---------------------------------------------------
 |
 | This file holds layer manager class.
 | It is used to create hotkeys and bind actions to them.
 |
 */

export default class {

	constructor (game, key, cb) {
		this.game = game
		this.key = key
		this.cb = cb

		this.action = game.input.keyboard.addKey(Phaser.Keyboard[key])

		if (cb)
			this.action.onDown.add(() => cb(), this)
	}

	bind (cb) {
		this.action.onDown.add(() => cb(), this)
	}

}
