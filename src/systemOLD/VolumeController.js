/**
 |---------------------------------------------------
 | VolumeController
 |---------------------------------------------------
 |
 | This file holds volume controller class.
 | @TODO Rework everything here.
 |
 */

import config from '../System/config'
const Path = config.path
const fs = require('fs')

/** Class representing volume controller */ 
class VolumeController {

	/**
	 * Creates a volume controller class.
	 * @param {object} game - Phaser's game object.
	 */
	constructor (game) {

		this.game = game

		this.isFading = false
		this.isVisible = false

		this.hideEvent = null
		this.soundEffect = null

		this.volume = this.game.userdata.sound.volume.master
		this.position = { 
			x: this.game.world.width - 150, 
			y: this.game.world.height - 350
		}

		this.sprites = []
		this.layers = []

		this.text_style = {
			font: "28px Lato",
			fill: "#ffffff",
			align: "center",
			stroke: 'rgba(0, 0, 0, 0.4)',
			strokeThickness: 5
		}

	}

	/**
	 * Runs every method used to create beautiful (I guess?) controller.
	 * @function
	 */
	create () {
		this.createSprites()
		this.applyProperties()
		this.setPositions()
		this.colorizeLayers()
		this.maskLayers()
		this.addScrollListener()
	}

	/**
	 * Create sprites and insert them into arrays.
	 */
	createSprites() {

		// Layers
		this.volume_layer1 = this.game.add.sprite(0, 0, 'volume_layer1')
		this.volume_layer4 = this.game.add.sprite(0, 0, 'volume_layer4')
		this.volume_layer5 = this.game.add.sprite(0, 0, 'volume_layer5')
		this.volume_layer2 = this.game.add.sprite(0, 0, 'volume_layer2')
		this.volume_layer3 = this.game.add.sprite(0, 0, 'volume_layer3')

		// Frame
		this.volume_frame = this.game.add.sprite(0, 0, 'volume_frame')

		// Value text
		this.volume_value = this.game.add.text(0, 0, this.volume + '%', this.text_style)

		// Controller text
		this.volume_category = this.game.add.text(0, 0, 'Master', this.text_style)

		// Frame mask
		this.mask = this.game.add.graphics(0, 0)
		this.mask.beginFill(0xffffff)
		this.mask.drawRect(0, 0, this.volume_frame.width - 15, this.volume_frame.height - 10)		

		// Sprite holders
		this.layers = [ this.volume_layer1, this.volume_layer2, this.volume_layer3, this.volume_layer4, this.volume_layer5 ]
		this.sprites = [ ...this.layers, this.volume_frame, this.volume_value, this.volume_category, this.mask ]

	}

	/**
	 * Applies properties to created sprites
	 */
	applyProperties () {

		this.volume_value.anchor.setTo(.5)
		this.volume_category.anchor.setTo(.5)
		this.sprites.forEach(sprite => sprite.alpha = 0 )

	}

	/**
	 * Sets position of every sprite used in the controller
	 */
	setPositions () {
		
		this.sprites.forEach(sprite => {
			sprite.x = this.position.x
			sprite.y = this.position.y
		})

		this.layers.forEach(layer => {
			layer.y = this.position.y + this.volume_frame.height - this.volume_frame.height / 100 * this.volume
		})

		this.mask.x += 10
		this.mask.y += 5

		this.volume_layer1.y -= 50

		this.volume_value.x = this.position.x + this.volume_frame.width / 2
		this.volume_value.y = this.position.y + this.volume_frame.height / 2
		this.volume_category.x = this.position.x + this.volume_frame.width / 2
		this.volume_category.y = this.position.y - 20

	}

	/**
	 * Sets tint of every layer used for the parallax effect in the controller
	 */
	colorizeLayers () {

		this.layers.forEach(layer => layer.tint = 0xf26596)

	}

	/**
	 * Masks parallax effect layers with created earlier mask
	 */
	maskLayers () {

		this.layers.forEach(layer => layer.mask = this.mask)

	}

	/**
	 * Updates text of the volume value sprite
	 */
	updateValueText () {

		this.volume_value.text = this.volume + '%'

	}

	/**
	 * Updates game audio volume to actual controller volume
	 */
	updateAudioVolume () {

		this.game.audio.volume = this.volume / 100

	}

	/**
	 * Save selected volume to settings json file
	 */
	updateUserSettings () {

		let userdataJSON
		this.game.userdata.sound.volume.master = this.volume
		userdataJSON = JSON.stringify(this.game.userdata, null, 4)

		fs.writeFile(Path.system + 'settings.json', userdataJSON, 'utf8', (err) => {
			if (err) console.log(err)
			else console.log('Userdata in settings.json updated!')
		})

	}

	/**
	 * Modifies controller volume value
	 * @param {number} val 
	 */
	modify (val) {

		this.factor = val > 0 ? 1 : -1

		if (this.factor == 1 && this.volume < 100)
			this.volume += val

		else if (this.factor == -1 && this.volume > 0)
			this.volume += val

		if (this.volume > 0 && this.volume < 100)
			this.moveLayers()

		if (this.volume % 4 == 0 && this.volume > 0 && this.volume < 100)
			this.playSound()

		this.updateValueText()
		this.updateAudioVolume()

	}

	/**
	 * Plays a volume change sound
	 */
	playSound () {

		this.soundEffect = this.game.add.audio('volume_change')
		this.soundEffect.volume = this.volume / 50
		this.soundEffect.play()

	}

	/**
	 * Moves layers according to their order and controler's volume value
	 */
	moveLayers () {

		let volumeTick = 2 * (this.volume_frame.height / 100)

		this.layers.forEach((layer, index) => {
			let pos = this.position.y + this.volume_frame.height - this.volume_frame.height / 100 * this.volume
			layer.y = pos - (this.factor * (index * 2))
		})

	}

	/**
	 * Display audio controller for short duration
	 */
	show () {

		if (this.isFading) return
		this.game.time.events.remove(this.hideEvent)

		let fadeIn
		this.isFading = true

		this.sprites.forEach(sprite => {
			fadeIn = this.game.add.tween(sprite).to({ alpha: 1 }, 500, "Sine.easeInOut", true)
		}, this)

		fadeIn.onComplete.add(() => {
			this.isFading = false
			this.isVisible = true
			this.hideEvent = this.game.time.events.add(750, () => this.hide(), this)
		})

	}

	/**
	 * Hide audio controller and save new audio value to userdata
	 */
	hide () {

		let fadeOut
		this.isFading = true

		this.sprites.forEach(sprite => {		
			fadeOut = this.game.add.tween(sprite).to({ alpha: 0 }, 500, "Sine.easeInOut", true)
		}, this)

		fadeOut.onComplete.add(() => {
			this.isFading = false
			this.isVisible = false

			this.updateUserSettings()
		})

	}

	addScrollListener () {

		this.game.input.mouse.mouseWheelCallback = (e) => {
			if (e.deltaY < 0) {
				this.show()
				this.modify(2)
			}
			if (e.deltaY > 0) {
				this.show()
				this.modify(-2)
			}			
		}

	}

	removeScrollListener () {

		this.game.input.mouse.mouseWheelCallback = (e) => {}

	}

}

export default VolumeController