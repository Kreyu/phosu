import HitCircle from '../objects/HitCircle'

export default class {
    
    constructor (game, diff) {
        this.game = game
        this.diff = diff
        this.hitObjects = diff.HitObjects

        this.time = 1500 - this.diff.Difficulty.ApproachRate * 100

        this.currentIndex = 0
        this.currentObject = null

        this.spawnedItems = []
    }

    loop () {
        this.currentObject = this.hitObjects[this.currentIndex]

        let gameTime = parseInt(this.game.audio.progress * 1000) - this.time
        let objTime = parseInt(this.currentObject.time) - (this.time * 2)

        if (gameTime >= objTime) {
            this.currentIndex++
            this.spawn()
        }
    }

    spawn () {
        let circle = new HitCircle(
            this.game, 
            this.game.world.width / 512 * this.currentObject.x, 
            this.game.world.height / 384 * this.currentObject.y, 
            this.time, 
            this.currentObject.type, 
            this.currentObject.hitSound
        )

        this.spawnedItems.push(circle)
    }

}