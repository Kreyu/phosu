export default class {
    
    constructor (game) {
        this.game = game
    }

    play (key, volume) {
        let sfx = this.game.add.audio(key)
        sfx.volume = this.game.userdata.sound.volume.master / 50
        sfx.play()
    }

}