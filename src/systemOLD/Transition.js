export default class {

    // Create instance with game object reference

    constructor (game) {
        this.game = game
    }

    // Reset camera and fade in

    fadeIn (duration = 200) {
		this.game.camera.resetFX()
		this.game.camera.flash(0x000000, duration)
    }

    // Reset camera and fade out, then change state to provided one's key

    fadeTo (state, duration = 450, params) {
        this.game.camera.resetFX()
        this.game.camera.fade(0x000000, duration)
        this.game.time.events.add(duration, () => this.game.state.start(state, true, false, params))        
    }
}