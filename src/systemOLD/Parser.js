const fs = require('fs')

class Parser {

    constructor () {

        this.beatmap = {}
        this.currentSection = null
        this.currentEvent = null

        this.bpm = {
            values: [],
            value: 0,
            min: 0,
            max: 0
        }

        this.quantity = {
            circle: 0,
            slider: 0,
            spinner: 0
        }

        this.sliderTypes = {
            C: 'catmull',
            B: 'bezier',
            L: 'linear',
            P: 'pass-through'
        }

        this.allowedExtensions = {
            background: ['jpg', 'png']
        }

    }

    parseFile (file, callback) {

        return new Promise((resolve, reject) => {

            fs.stat(file, err => {

                if (err)throw new Error(err)

                let stream = fs.createReadStream(file)

                stream.on('data', chunk => {
                    let buffer = chunk.toString('utf8')
                    let lines = buffer.split(/\r?\n/)
                    lines.forEach(this.readLine, this)
                })

                stream.on('error', reject)

                stream.on('end', () => {
                    this.parseEvents()
                    this.beatmap.Counters = this.quantity
                    let bpm = {
                        min: Math.min(...this.bpm.values),
                        max: Math.max(...this.bpm.values)
                    }

                    this.beatmap.BPM = bpm
                    resolve(this.beatmap)
                })

            }, this)

        })
    }

    readLine (line) {

        line = line.trim()
        if (!line) return

        let section = line.match(/^\[([a-zA-Z0-9]+)\]$/)
        if (section) {
            section = section[1]
            this.currentSection = section
            this.beatmap[section] = {}
            return
        }

        let format = line.match(/^osu file format (v[0-9]+)$/)
        if (format) {
            format = format[1]
            this.beatmap.Format = format
            return
        }

        let property = line.match(/^([a-zA-Z0-9]+)[ ]*:(?:[ ]*(.+)$)?/)
        if (property) {
            this.beatmap[this.currentSection][property[1]] = property[2] || null
            return
        }

        if (!Array.isArray(this.beatmap[this.currentSection]))
            this.beatmap[this.currentSection] = []

        this.beatmap[this.currentSection].push(this['read' + this.currentSection].call(this, line))

    }

    readHitObjects (hitObject) {

        let [ x, y, time, type, hitSound, addition, endTime ] = hitObject.split(',')

        let newCombo = (type & 4) == 4
        type = this.readType(type)

        if (type == 'spinner')
            [ addition, endTime ] = [ endTime, addition ]
        
        hitSound = this.readHitSound(hitSound)
        addition = type == 'slider' ? this.readSlider(addition) : this.readAddition(addition)
        
        hitObject = { x: x, y: y, time: time, type: type, hitSound: hitSound, newCombo: newCombo }

        if (type == 'circle') hitObject['addition'] = addition
        if (type == 'slider') hitObject['slider'] = addition
        if (type == 'spinner') {
            hitObject['endTime'] = endTime
            hitObject['addition'] = addition
        }

        return hitObject

    }
    
    readTimingPoints (hitObject) {

        let [ offset, msperbeat, meter, sampletype, sampleset, volume, inherited, kiaimode ] = hitObject.split(',')
        let velocity = 1
        msperbeat = parseFloat(msperbeat)

        if (!isNaN(msperbeat) && msperbeat !== 0) {
            if (msperbeat > 0) {
                let bpm = msperbeat
                this.bpm.value = bpm
                this.bpm.values.push(bpm / meter)
            }
        }

        return { 
            offset: offset,
            msperbeat: msperbeat,
            meter: meter,
            bpm: this.bpm.value,
            velocity: velocity,
            sampletype: sampletype,
            sampleset: sampleset,
            volume: volume,
            inherited: inherited,
            kiaimode: kiaimode
        }

    }

    readType (type) {

        let object

        if ((type & 1) == 1) object = 'circle'
        else if ((type & 2) == 2) object = 'slider'
        else if ((type & 8) == 8) object = 'spinner'

        this.quantity[object]++
        return object

    }

    readHitSound (hitSound) {

        let soundTypes = []

        if ((hitSound & 2) == 2) soundTypes.push('whistle')
        if ((hitSound & 4) == 4) soundTypes.push('finish')
        if ((hitSound & 8) == 8) soundTypes.push('clap')
        if (soundTypes.length === 0) soundTypes.push('normal')

        return soundTypes

    }

    readAddition (addition) {

        if (!addition) return {}

        let [ sampleSet, additions, customIndex, sampleVolume, filename ] = addition.split(':')

        return { 
            sampleSet: sampleSet,
            additions: additions,
            customIndex: customIndex,
            sampleVolume: sampleVolume,
            filename: filename
        }

    }

    readSlider (slider) {

        if (!slider) return {}
        slider = slider.split('|')

        let sliderType = slider[0]
        let [ curvePoints, repeat, pixelLength, edgeHitsounds, edgeAdditions, addition ] = slider.slice(1).join('|').split(",")

        if (curvePoints) {
            let points = []
            curvePoints = curvePoints.split('|')
            curvePoints.forEach(point => {
                point = point.split(':')
                points.push({ x: point[0], y: point[1] })
            })
            curvePoints = points
        }

        if (edgeHitsounds) {
            let hitSounds = []
            edgeHitsounds = edgeHitsounds.split('|')
            edgeHitsounds.forEach(edge => hitSounds.push(this.readHitSound(edge)))
            edgeHitsounds = hitSounds
        }

        if (edgeAdditions) {
            let additions = []
            edgeAdditions = edgeAdditions.split('|')
            edgeAdditions.forEach(edge => additions.push(this.readAddition(edge)))
            edgeAdditions = additions 
        }

        if (addition) {
            addition = addition.split('|')
            addition = this.readAddition(addition[0])
        }

        return {
            sliderType: sliderType,
            curvePoints: curvePoints,
            repeat: repeat,
            pixelLength: pixelLength,
            edgeHitsounds: edgeHitsounds,
            edgeAdditions: edgeAdditions,
            addition: addition
        }

    }

    readEvents (hitObject) {

        return hitObject

    }

    parseEvents () {

        let events = {}
        let backgrounds = []

        this.beatmap.Events.forEach(event => {

            let section = event.match(/^\/\/([a-zA-Z0-9 ()]+)$/)
            if (section) {
                this.currentEvent = section[1]
                events[this.currentEvent] = []
                return
            }

            let file = event.match(/"([^"]*)"/)
            if (file) {
                file = file[1]
                let extensionCheck = this.allowedExtensions.background.some(ext => file.includes(ext))
                if (extensionCheck) {
                    backgrounds.push(file.replace(/^"(.*)"$/, '$1'))
                }
            }

            events[this.currentEvent].push(event)
        }, this)

        this.beatmap.Events = events
        this.beatmap.Metadata.Background = backgrounds.length <= 1 ?
             backgrounds[0] || null : 
             backgrounds
            
        return this.beatmap
    }

}

export default { 
    async parseFile (file) {
        let parser = new Parser()
        return await parser.parseFile(file)
    }
}
