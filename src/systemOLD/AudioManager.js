/**
 * AUDIO MANAGER
 * Used to manage beatmaps audio and send information to main audio controller
 */

import Path from './Path'

export default class {

	constructor (game) {
		this.game = game
		this.beatmap = null
		this.beatmapID = null
	}

	//

	play () {
		this.game.audio.play()
		return this
	}

	pause () {
		this.game.audio.pause()
		return this
	}

	stop () {
		this.game.audio.stop()
		return this
	}

	next () {
		this.stop()

		this.beatmapID++

		if (this.beatmapID > this.game.beatmaps.length - 1)
			this.beatmapID = 0

		this.beatmap = this.game.beatmaps[this.beatmapID]

		this.load()
		this.play()
		return this
	}

	previous () {
		this.stop()

		this.beatmapID--

		if (this.beatmapID < 0)
			this.beatmapID = this.game.beatmaps.length - 1

		this.beatmap = this.game.beatmaps[this.beatmapID]

		this.load()
		this.play()
		return this
	}

	load (beatmap) {
		if (beatmap !== undefined)
			this.beatmap = beatmap

		console.log(this.beatmap)
		let audio = this.beatmap.resources
			.filter(resource => resource.split('.').pop() === 'mp3')
		
		this.game.audio.src = Path.songs + '\\' + this.beatmap.folder + '\\' + audio
		return this
	}

	preview () {
		let progress = this.beatmap.difficulties[0].General.PreviewTime
		this.game.audio.progress = progress / 1000
		
		return this
	}

	pick () {
		let mapID = Math.floor(Math.random() * this.game.beatmaps.length)
		let map = this.game.beatmaps[mapID]

		this.beatmapID = mapID
		this.beatmap = map

		return this
	}

	loop (preview) {
		this.game.audio.on('end', audio => {
			if (preview) 
				this.preview()
		}, this)
	}
}
