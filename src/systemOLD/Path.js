/**
 |---------------------------------------------------
 | Path manager
 |---------------------------------------------------
 |
 | This file holds paths for project after the build
 | Every path should be defined here
 |
 */

const path = require('path')
const base = path.resolve(process.cwd() + '/static/') + '\\'

export default {

	logs: 		base + 'Logs\\',
	system: 	base + 'System\\',
	skins: 		base + 'Skins\\',
	songs: 		base + 'Songs\\',
	assets:		base + 'System\\Assets\\'

}
