import MusicPlayer from './../Components/MusicPlayer/MusicPlayer'
import Background from '../Components/Background/Background'
import Spectrum from '../objects/Spectrum'
import Overlay from '../objects/Overlay'
import Cookie from '../objects/Cookie'

import Parallax from '../System/VFX/Parallax'
import ParallaxEnum from '../System/Enum/ParallaxEnum';

export default class extends Phaser.State {

	init () {
		this.Parallax = new Parallax(this.game)
		this.Background = new Background(this.game)
		this.MusicPlayer = new MusicPlayer(this.game)
		// this.VolumeController = new VolumeController(this.game)
		// this.Transition = new Transition(this.game)
		// this.Background = new Background(this.game)
		// this.Parallax = new Parallax(this.game)
		// this.Spectrum = new Spectrum(this.game)
		// this.Overlay = new Overlay(this.game)
		// this.SoundFX = new SoundFX(this.game)
		// this.Cookie = new Cookie(this.game)
	}

	create () {
		this.Background.create()
		this.MusicPlayer.create()

		this.Parallax.add([
			{
				target: this.Background.getContainer(), 
				anchor: ParallaxEnum.ANCHOR_CENTER, 
				intensity: 100
			}
		])

		this.Parallax.apply()

		// this.game.audio.play()

		// if (!this.game.audio.isPlaying) {
		// 	this.game.audio.manager.pick().load().play()
		// 	this.game.audio.volume = this.game.userdata.sound.volume.master / 100
		// } 

		// this.Background.create()
	  	// this.Spectrum.create()
		// this.Overlay.create()
		// this.Cookie.create()
		// this.MusicPlayer.create()
		// this.VolumeController.create()

		// if (this.game.audio.isPlaying) 
		// 	this.MusicPlayer.updateProgress()

		// this.Overlay.setAuthor('2017, Kreyu')
		// this.Overlay.setVersion('ver. 1.00-dev')

		// this.Parallax.add(this.Background, 'center', 3)
		// this.Parallax.add(this.Cookie, 'center', 1)
		// this.Parallax.add(this.Spectrum, 'center', 1)

		// this.Cookie.on('click', () => {
		// 	this.SoundFX.play('menuhit', this.game.userdata.sound.volume.master / 50)

		// 	this.Cookie.close()
		// 	this.VolumeController.removeScrollListener()
		// 	this.Transition.fadeTo('SelectState')
		// })

		// this.currentInterval = null
		// this.Parallax.run()

		// new Hotkey(this.game, 'ESC', () => {
		// 	console.log('Test')
		// })
	}

	update () {
		this.MusicPlayer.update()
		// this.game.audio.analyze()

		// let beatmap = this.game.audio.manager.beatmap.difficulties[0]
		// let timingPoints = beatmap.TimingPoints.filter(timingPoint => parseInt(timingPoint.offset) < this.game.audio.progress * 1000)
		// let timingPoint = timingPoints[timingPoints.length - 1]

		// if (timingPoint !== undefined) {
		// 	let BPM = timingPoint.bpm * timingPoint.meter

		// 	if (this.BPM !== BPM) {
		// 		this.BPM = BPM
		// 		this.game.time.events.remove(this.BPM_Loop)
		// 		this.BPM_Loop = this.game.time.events.loop(this.BPM, () => {
		// 			if (this.game.audio.isPlaying) {
		// 				this.Overlay.flash(10, 350)
		// 				this.Cookie.beat()
		// 				this.Cookie.createRing(this.BPM, timingPoint.bpm)
		// 			}
		// 		})
		// 	}
		// }

		// this.Spectrum.update(this.Cookie.logo)
		// this.Cookie.pulse()
	}

	paused () {
		// this.game.audio.pause()
	}

	resumed () {
		// this.game.audio.play()
	}

}
