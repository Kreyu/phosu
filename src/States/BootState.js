import BeatmapsLoader from '../System/Loaders/BeatmapsLoader'
import UserdataLoader from '../System/Loaders/UserdataLoader'
import ScriptsLoader from '../System/Loaders/ScriptsLoader'
import WebfontLoader from '../System/Loaders/WebfontLoader'
import AssetsLoader from '../System/Loaders/AssetsLoader'
import SkinsLoader from '../System/Loaders/SkinsLoader'
import { ipcRenderer } from 'electron'

export default class BootState extends Phaser.State {

	async init () {

		// Initialize loaders
		this.Loaders = {
			beatmaps: new BeatmapsLoader(this.game),
			userdata: new UserdataLoader(this.game),
			scripts: new ScriptsLoader(this.game),
			webfont: new WebfontLoader(this.game),
			assets: new AssetsLoader(this.game),
			skins: new SkinsLoader(this.game)
		}
	}

	async preload () {

		// Execute loaders
		for (let name in this.Loaders) {
			if (name !== 'assets') {
				await this.Loaders[name].load()
			}
		}

		// Load assets
		await this.Loaders.assets.load(

			// Load skins assets
			this.Loaders.skins.skins
		)
		
		this.game.managers.userdata.collection = this.Loaders.userdata.userdata
		this.game.managers.beatmaps.collection = this.Loaders.beatmaps.beatmaps
		this.game.managers.skins.collection = this.Loaders.skins.skins
	}

	async create () {

		// Calculate progress
		this.progress = 0

		for (let name in this.Loaders) {
			this.Loaders[name].onProgress.add(progress => {
				this.progress += progress
				const overallProgress = this.progress / 6

				if (overallProgress === 100) {
					ipcRenderer.send('loading-complete')
					return this.game.state.start('MenuState', true, false)
				}

				ipcRenderer.send('loading-progress', overallProgress)
			})
		}
	}

}
