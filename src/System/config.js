import path from 'path'

const base = path.resolve(process.cwd() + '/static/') + '\\'

export default {

	visuals: {
		menu: {
			background: 'background'
		}
	},

	path: {
		logs: 		base + 'Logs\\',
		system: 	base + 'System\\',
		skins: 		base + 'Skins\\',
		songs: 		base + 'Songs\\',
		assets:		base + 'System\\Assets\\'
	}

}
