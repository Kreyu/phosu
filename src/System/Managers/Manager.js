export default class Manager {
    constructor (game) {
        this._game = game
        this._collection = []
    }

    set collection (collection) {
        this._collection = collection
    }
}