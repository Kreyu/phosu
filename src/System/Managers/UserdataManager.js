import Manager from "./Manager";

export default class UserdataManager extends Manager {
    constructor (game) {
        super(game)
    }

    get (group, key, subkey = null) {
        const result = this._collection[group][key]

        return subkey ? 
            result[subkey] : result
    }
}