import Manager from "./Manager";
import NoBeatmapSelectedException from "../Exceptions/NoBeatmapSelectedException";

export default class BeatmapManager extends Manager {
    
    constructor (game) {
        super(game)

        this._currentBeatmap = null
        this._currentDifficulty = null
    }

    get currentBeatmap () {
        return this._currentBeatmap
    }

    get currentDifficulty () {
        return this._currentDifficulty
    }

    get currentTitle () {
        return this._currentDifficulty.Title
    }

    pick () {
        this.pickBeatmap()
        this.pickDifficulty()
    }

    /**
     * Picks random beatmap from collection.
     */
    pickBeatmap () {
        this._currentBeatmap = this._collection[Math.floor(
            Math.random() * this._collection.length
        )]
    }

    /**
     * Picks random difficulty from current selected beatmap.
     * 
     * @throws NoBeatmapSelectedException()
     */
    pickDifficulty () {
        if (this._currentBeatmap === null) {
            throw new NoBeatmapSelectedException()
        }

        this._currentDifficulty = this._currentBeatmap.pickDifficulty()
    }

    /**
     * Changes and loads current difficulty's into audio system.
     */
    load () {
        const audioFilename = this.currentDifficulty.AudioFilename
        const source = this.currentBeatmap.fullpath + '\\' + audioFilename

        this._game.audio.changeSource(source)
    }

    /**
     * Changes current beatmap to next from collection.
     */
    next () {
        const currentIndex = this._collection.indexOf(this._currentBeatmap)
        let newIndex = currentIndex + 1

        if (!(newIndex in this._collection)) {
            newIndex = 0
        }

        this._currentBeatmap = this._collection[newIndex]
    }

    /**
     * Changes current beatmap to previous from collection.
     */
    previous () {
        const currentIndex = this._collection.indexOf(this._currentBeatmap)
        let newIndex = currentIndex - 1

        if (!(newIndex in this._collection)) {
            newIndex = 0
        }

        this._currentBeatmap = this._collection[newIndex]
    }

}