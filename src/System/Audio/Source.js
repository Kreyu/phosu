export default class Source {

    constructor (path, filename) {
        this._path = path
        this._filename = filename
    }
    
    get path () {
        return this._path
    }

    get filename () {
        return this._filename
    }

}