import Analyser from './Analyser'

/**
 * @class
 * @extends Analyser
 * @classdesc Class representing an engine audio system.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Audio extends Analyser {

    /**
     * Audio system constructor.
     * 
     * @param {String} id Audio element id
     */
    constructor (id) {
        super()

        /** @protected */
        this._audioElement = document.getElementById(id)

        this._createAnalyser()
    }

    /**
     * Returns current time of audio element.
     * 
     * @returns {Number}
     */
    get currentTime () {
        return this._audioElement.currentTime
    }

    /**
     * Sets new current time value to audio element.
     * 
     * @param {Number} value New currentTime value.
     * @returns {Audio}
     */
    set currentTime (value) {
        this._audioElement.currentTime = value

        return this
    }

    /**
     * Returns curation of audio element source.
     * 
     * @returns {Number}
     */
    get duration () {
        return this._audioElement.duration
    }

    /**
     * Returns audio element's volume.
     * 
     * @returns {Number}
     */
    get volume () {
        return this._audioElement.volume
    }

    /**
     * Sets new audio element's volume.
     * 
     * @param {Number} volume
     */
    set volume (volume) {
        this._audioElement.volume = volume
    }

    /**
     * Plays audio element playback.
     */
    play () {
        this._audioElement.play()
    }

    /**
     * Pauses audio element playback.
     */
    pause () {
        this._audioElement.pause()
    }

    /**
     * Stops audio element playback.
     */
    stop () {
        this._audioElement.pause()
        this._audioElement.currentTime = 0
    }

    /**
     * Replays audio element playback.
     */
    replay () {
        this._audioElement.pause()
        this._audioElement.currentTime = 0
        this._audioElement.play()
    }

    /**
     * Changes audio element source and loads it.
     * 
     * @param {String} source 
     */
    changeSource (source) {
        this._audioElement.src = source
        this._audioElement.load()
    }

    /**
     * Returns if audio element is ready to play.
     * 
     * @returns {Boolean}
     */
    isReady () {
        return this._audioElement.readyState === 4
    }

    /**
     * Returns if audio element is playing.
     * 
     * @returns {Boolean}
     */
    isPlaying () {
        return !this._audioElement.paused
    }

    /**
     * Returns if audio element is paused.
     * 
     * @returns {Boolean}
     */
    isPaused () {
        return this._audioElement.paused
    }

    /**
     * Returns if audio element is stopped.
     * 
     * @returns {Boolean}
     */
    isStopped () {
        return this._audioElement.paused &&
            !this._audioElement.currentTime
    }

    /**
     * Returns if audio element has finished playing.
     * 
     * @returns {Boolean}
     */
    hasFinished () {
        return this._audioElement.currentTime ===
            this._audioElement.duration
    }

}