import AnalyserEnum from './../Enum/AnalyserEnum'

/**
 * @class
 * @classdesc Abstract class that represents audio analyser
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Analyser {

    constructor () {

        /** @private */
        this._sourceNode = null

        /** @private */
        this._analyserNode = null

        /** @private */
        this._scriptProcessor = null

        /** @private */
        this._frequencies = null

        /** @private */
        this._context = new AudioContext()
    }

    /**
     * Creates audio analyser.
     * 
     * @protected
     */
    _createAnalyser () {
        this._setupSourceNode()
        this._setupAnalyserNode()
        this._setupScriptProcessor()
    }    

    /**
     * Creates source node and connects it to context destination.
     * 
     * @private
     */
    _setupSourceNode () {
        this._sourceNode = this._context.createMediaElementSource(this._audioElement)
        this._sourceNode.connect(this._context.destination)
    }

    /**
     * Creates analyser node and configures its properties.
     * 
     * @private
     */
    _setupAnalyserNode () {
        this._analyserNode = this._context.createAnalyser()
        this._analyserNode.smoothingTimeConstant = AnalyserEnum.SMOOTHING_TIME_CONSTANT
        this._analyserNode.fftSize = AnalyserEnum.FFT_SIZE

        this._sourceNode.connect(this._analyserNode)
    }

    /**
     * Creates script processor and creates on audio progress callback.
     * 
     * @private
     */
    _setupScriptProcessor () {
        this._scriptProcessor = this._context.createScriptProcessor(
            AnalyserEnum.BUFFER_SIZE, 
            AnalyserEnum.INPUT_CHANNELS,
            AnalyserEnum.OUTPUT_CHANNELS)

        this._scriptProcessor.onaudioprocess = this._onAudioProcess.bind(this)
        this._scriptProcessor.connect(this._context.destination)
    }

    /**
     * Method which is being called on script processor's audio process.
     * 
     * @private
     */
    _onAudioProcess () {
        const frequencies = new Uint8Array(this._analyserNode.frequencyBinCount)
        this._analyserNode.getByteFrequencyData(frequencies)
        this._frequencies = frequencies
    }

    /**
     * Returns average frequency of current audio byte.
     * 
     * @returns {Number}
     */
    get averageFrequency () {
        let sum = 0,
            average = 0,
            length = this._frequencies.length
        
        for (i in this._frequencies) {
            sum += this._frequencies[i]
        }

        average = sum / length
        return average
    }    

}
