import VFX from "./VFX";
import NoParallaxTargetGivenException from "../Exceptions/NoParallaxTargetGivenException";
import ParallaxEnum from "../Enum/ParallaxEnum";

export default class Parallax extends VFX {

    constructor (game) {
        super(game)

        this._sources = []
    }

    add (sources) {
        sources.forEach(source => 
            this._processSource(source))
    }

    apply () {
        this._game.input.addMoveCallback((pointer, x, y) => {

            for (let source of this._sources) {
                const anchor = this._processAnchor(source.anchor)
                const offset = {
                    x: (this._game.world.centerX - x) / source.intensity,
                    y: (this._game.world.centerY - y) / source.intensity }

                source.sprite.x = anchor.x + offset.x
                source.sprite.y = anchor.y + offset.y
            }
        })
    }

    _processSource (source) {
        if (!source.target) {
            throw new NoParallaxTargetGivenException() 
        }
        console.log(source)

        const target = source.target
        const anchor = source.anchor || ParallaxEnum.ANCHOR_CENTER
        const intensity = source.intensity || 5

        target.children.forEach(group => {
            group.forEach(sprite => {
                this._sources.push({
                    sprite, 
                    anchor, 
                    intensity
                })
            })
        })
    }

    _processAnchor (anchor) {
        switch (anchor) {
            case ParallaxEnum.ANCHOR_CENTER:
                return {
                    x: this._game.world.centerX,
                    y: this._game.world.centerY }

                break
        }
    }

}