import { stripIndent } from 'common-tags'

export default class NoParallaxTargetGivenException extends Error {

    constructor (...args) {
        super(...args)

        this.message = stripIndent`
            No parallax target was specified when using add() method.`

        Error.captureStackTrace(this, NoParallaxTargetGivenException)
    }

}
