import { stripIndent } from 'common-tags'

export default class NoBeatmapSelectedException extends Error {

    constructor (...args) {
        super(...args)

        this.message = stripIndent`
            No beatmap was picked with the BeatmapManager.
            Use manager's pickBeatmap() method first.`

        Error.captureStackTrace(this, NoBeatmapSelectedException)
    }

}
