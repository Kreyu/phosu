export default class AnalyserEnum {

    /** @static */
    static get INPUT_CHANNELS () {
        return 1
    }

    /** @static */
    static get OUTPUT_CHANNELS () {
        return 2
    }

    /** @static */
    static get SMOOTHING_TIME_CONSTANT () {
        return 0.3
    }

    /** @static */
    static get FFT_SIZE () {
        return 1024
    }

    /** @static */
    static get BUFFER_SIZE () {
        return 2048
    }

}