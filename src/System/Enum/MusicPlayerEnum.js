export default class MusicPlayerEnum {

    /** @static */
    static get STATE_PLAYING () { 
        return 1 
    }

    /** @static */
    static get STATE_PAUSED () { 
        return 2 
    }

    /** @static */
    static get STATE_STOPPED () { 
        return 3 
    }

}