import config from './../config'

export default class Skin {

    constructor (data) {
        this._resources = data.resources
        this._config = data.config
        this._folder = data.folder
    }

    get config () {
        return this._config
    }

    get folder () {
        return this._folder
    }

    get resources () {
        return this._resources
    }

    get fullPath () {
        return config.path.skins + this._folder
    }

}