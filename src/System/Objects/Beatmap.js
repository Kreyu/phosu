import config from "../config";

export default class Beatmap {

    constructor (data) {
        this._difficulties = data.difficulties
        this._resources = data.resources
        this._folder = data.folder
    }

    get difficulties () {
        return this._difficulties
    }

    getDifficulties () {
        return this._difficulties
    }

    addDifficulty (difficulty) {
        this._difficulties.push(difficulty)
    }

    setDifficulties (difficulties) {
        this._difficulties = difficulties
    }

    getResources () {
        return this._resources
    }

    addResource (resource) {
        this._resources.push(resource)
    }

    setResources (resources) {
        this._resources = resources
    }

    setFolder (folder) {
        this._folder = folder
    }

    getFolder () {
        return this._folder
    }

    get folder () {
        return this._folder
    }

    get fullpath () {
        return config.path.songs + this._folder
    }

    pickDifficulty () {
        return this._difficulties[Math.floor(
            Math.random() * this._difficulties.length
        )]
    }
    
}