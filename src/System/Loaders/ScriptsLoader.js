import Loader from './Loader'
import fs from 'fs'

/**
 * @class
 * @classdesc Class representing a game engine scripts loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class ScriptsLoader extends Loader {

    /**
     * Scripts loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {
        super(game)
    }

    /**
     * Executes loader.
     * 
     * @async
     * @returns {Promise}
     */
    async load () {
        this._game.load.script('BlurX', 'https://cdn.rawgit.com/photonstorm/phaser-ce/master/filters/BlurX.js')
        this._game.load.script('BlurY', 'https://cdn.rawgit.com/photonstorm/phaser-ce/master/filters/BlurY.js')

        return new Promise((resolve, reject) => {
            this._game.load.onFileComplete.add(progress => this.setProgress(progress), this)
            this._game.load.onLoadComplete.add(() => {
                this.setReady()
                resolve()
            }, this)

            this._game.load.start()
        })
    }

}
