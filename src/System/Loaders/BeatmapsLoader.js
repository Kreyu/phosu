import Beatmap from './../Objects/Beatmap'
import config from './../config'
import Parser from 'osu-parser'
import Loader from './Loader'
import fs from 'fs'

/**
 * @class
 * @classdesc Class representing a game engine beatmaps loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class BeatmapsLoader extends Loader {

    /**
     * Beatmaps loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {
        super(game)

		this.beatmaps = []
		this.allowedFormats = ['png', 'jpg', 'mp3', 'wav', 'avi', 'flv']
    }

    /**
     * Executes loader.
     * 
     * @async
     */
    async load () {
		const folders = await fs.readdirSync(config.path.songs, 'utf8')
		let loadedCount = 0

		for (const folder of folders) {
			const path = config.path.songs + '\\' + folder
			const files = await fs.readdirSync(path, 'utf8')

			let difficulties = []
			let resources = []

			for (const index in files) {
				const file = files[index]
				const format = file.split('.').pop()

				if (format === 'osu') {
					let a = Parser.parseFile(path + '\\' + file, (err, res) => {
						if (err) return;

						res.file = file
						difficulties.push(res)
					})
					
					continue;
				}

				resources.push(file)
			}

			this.beatmaps.push(new Beatmap({
				difficulties, 
				resources, 
				folder
			}))

			this.setProgress((100 / folders.length) * ++loadedCount)
		}

		this._game.beatmaps = this.beatmaps
		this.setReady()
    }

}
