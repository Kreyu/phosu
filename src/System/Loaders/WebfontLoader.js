import Webfont from 'webfontloader'
import Loader from './Loader'

/**
 * @class
 * @classdesc Class representing a game engine webfont loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class WebfontLoader extends Loader {

    /**
     * Webfont loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {
        super(game)

        this._families = [
            'Titillium Web',
            'Roboto',
            'Lato',
        ]
    }

    /**
     * Executes loader.
     * 
     * @async
     * @returns {Promise}
     */
    async load () {
        return new Promise((resolve, reject) => {
            Webfont.load({
                active: () => {
                    this.setReady()
                    this.setProgress(100)

                    resolve()
                },

                google: {
                    families: this._families
                },

                timeout: 1000
            })
        })
    }

}
