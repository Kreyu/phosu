import config from './../config'
import Loader from './Loader'
import fs from 'fs'

/**
 * @class
 * @classdesc Class representing a game engine userdata loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class UserdataLoader extends Loader {

    /**
     * Userdata loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {
        super(game)

		this.userdata = null
    }

    /**
     * Executes loader.
     * 
     * @async
     * @returns {Promise}
     */
    async load () {
		let settings = await fs.readFileSync(config.path.system + '\\' + 'settings.json', 'utf8')
		this.userdata = JSON.parse(settings)

		this.setProgress(100)
		this.setReady()
    }

}
