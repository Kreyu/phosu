/**
 * @class
 * @abstract
 * @classdesc Abstract class representing a game engine loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Loader {

    /** @static */
    static get STATE_NOT_READY () {
        return 0
    }

    /** @static */
    static get STATE_READY () {
        return 1
    }

    /**
     * Loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {

        /** @private */
        this._game = game

        /** @private */
        this._state = Loader.STATE_NOT_READY

        /** @private */
        this._progress = 0

        /** @public */
        this.onReady = new Phaser.Signal()

        /** @public */
        this.onProgress = new Phaser.Signal()
    }

    /**
     * Returns loader state.
     * 
     * @returns {Boolean}
     */
    isReady () {
        return this._state === Loader.STATE_READY
    }

    /**
     * Sets loader state to ready.
     * Dispatches onReady signal.
     * 
     * @returns {Boolean}
     */    
    setReady () {
        this._state = Loader.STATE_READY
        this.onReady.dispatch()
    }

    /**
     * Sets loader state to not ready.
     * 
     * @returns {Boolean}
     */
    setNotReady () {
        this._state = Loader.STATE_NOT_READY
    }

    /**
     * Returns loader progress.
     * 
     * @returns {Number}
     */
    getProgress () {
        return this._progress
    }

    /**
     * Sets loader progress value.
     * Dispatches onProgress signal.
     * 
     * @param {*} progress 
     */
    setProgress (progress) {
        let before = this._progress
        this._progress = progress

        this.onProgress.dispatch(progress - before)
    }

}
