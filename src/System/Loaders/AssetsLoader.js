import config from './../config'
import Loader from './Loader'
import fs from 'fs'

/**
 * @class
 * @classdesc Class representing a game engine assets loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class AssetsLoader extends Loader {

    /**
     * Assets loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {
        super(game)

        this.assets = {}
        this.allowedFormats = {
            image: ['png', 'jpg', 'jpeg'],
            audio: ['mp3', 'wav'],
            video: ['avi', 'flv', 'webm']
        }
    }

    /**
     * Executes loader.
     * 
     * @async
     */
    async load (skins) {
        // TODO: change
        let skinName = 'skin'

        for (let skin of skins)
            if (skin.name == skinName)
                this.assets.skin = skin.resources

        this.assets.system = await fs.readdirSync(config.path.assets, 'utf8')

        for (let assetPack of Object.keys(this.assets)) {
            for (let asset of this.assets[assetPack]) {
                let name = asset.replace(/\.[^/.]+$/, "")
                let ext = asset.split('.').pop()
                let path = assetPack == 'system' ?
                    `${config.path.assets}\\${asset}` :
                    `${config.path.skins}\\${skinName}\\${asset}`
    
                if (this.allowedFormats.image.includes(ext))
                    this._game.load.image(name, path)
    
                else if (this.allowedFormats.audio.includes(ext))
                    this._game.load.audio(name, path)
    
                else if (this.allowedFormats.video.includes(ext))
                    this._game.load.video(name, path)     
            }
        }

        this._game.load.onFileComplete.add(progress => this.setProgress(progress), this)
        this._game.load.onLoadComplete.add(() => this.setReady(), this)

        this._game.load.start()
    }

}
