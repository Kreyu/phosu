import { ConfigIniParser } from 'config-ini-parser'
import Skin from './../Objects/Skin'
import config from './../config'
import Loader from './Loader'
import fs from 'fs'

/**
 * @class
 * @classdesc Class representing a game engine skins loader.
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class SkinsLoader extends Loader {

    /**
     * Skins loader class constructor.
     * 
     * @constructor
     * @param {Phaser.Game} game Main game instance
     */
    constructor (game) {
        super(game)

        this.skins = []
        
		this.allowedFormats = ['png', 'jpg', 'mp3', 'wav']
    }

    /**
     * Executes loader.
     * 
     * @async
     */
    async load () {
        let parser = new ConfigIniParser()
        let folders = await fs.readdirSync(config.path.skins, 'utf8')
        let loadedCount = 0

		for (let [index, folder] of folders.entries()) {
			let ini = await fs.readFileSync(config.path.skins + '\\' + folder + '\\' + 'skin.ini', 'utf8')
			let resources = await fs.readdirSync(config.path.skins + '\\' + folder, 'utf8')
                .filter(filename => this.allowedFormats.includes(filename.split('.').pop()))
            
            this.skins.push(new Skin({
                config: parser.parse(ini),
                resources: resources,
                folder: folder
            }))

            this.setProgress((100 / folders.length) * ++loadedCount)
		}
        
		this.setReady()
    }

}
