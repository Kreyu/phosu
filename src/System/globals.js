export default (function () {

    /**
     * Capitalizes first letter of the string.
     * 
     * @returns {String}
     */
    String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.slice(1)
    }

    /**
     * Calls a function on each child in this group.
     * Additionally, returns loop index.
     * 
     * @param {Function} callback 
     * @param {undefined|Object} callbackContext 
     * @param {undefined|Boolean} checkExists 
     */
    Phaser.Group.prototype.forEach = function (callback, callbackContext, checkExists) {
        if (checkExists === undefined) { 
            checkExists = false
        }
    
        if (arguments.length <= 3) {
            for (var i = 0; i < this.children.length; i++) {
                if (!checkExists || (checkExists && this.children[i].exists)) {
                    callback.call(callbackContext, this.children[i], i)
                }
            }
        } else {
            // Assigning to arguments properties causes Extreme Deoptimization in Chrome, FF, and IE.
            // Using an array and pushing each element (not a slice!) is _significantly_ faster.
            var args = [null]
    
            for (var i = 3; i < arguments.length; i++) {
                args.push(arguments[i])
            }
    
            for (var i = 0; i < this.children.length; i++) {
                if (!checkExists || (checkExists && this.children[i].exists)) {
                    args[0] = this.children[i]
                    args[1] = i
                    callback.apply(callbackContext, args)
                }
            }
        }
    }

})()
