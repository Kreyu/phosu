![phosu! logo](https://i.imgur.com/vN8Y9NN.png)

An osu! game clone, made using Phaser + Electron.  
Project for educational purposes, to learn OOP Javascript with ES6+ standards.

<dl>
  <a href='https://i.imgur.com/C9Ve0T3.jpg'>
    <img src='https://i.imgur.com/C9Ve0T3.jpg' width='250px'></img>
  </a>

  <a href='https://i.imgur.com/UQFItEp.jpg'>
    <img src='https://i.imgur.com/UQFItEp.jpg' width='250px'></img>
  </a>
</dl>

# Build from source
**Prerequisites**  
[windows-build-tools](https://github.com/nodejs/node-gyp#option-1) if you're on windows machine and doesn't have it already.

```
git clone https://gitlab.com/Kreyu/phosu
cd phosu
npm install
npm start
```

**Important!**  
In order to test game, you **have** to put some beatmaps and skins.  
To get a song, download it [from official osu! website](https://osu.ppy.sh/).  
Skins are available in osu! community forums.  
Put them in `./static/Songs` and `./static/Skins` folders, and you're ready to go.

**Note:** this is only needed temporarily. Soon project will initially contain some songs and skins.

# Structure and states

**DON'T MIND THIS SECTION AS I AM CHANGING WHOLE FILE/STATE STRUCTURE**
Every game state has separate folder inside `src` directory.

`Boot` - state used to load all assets - system sprites, user skins and songs.  
Progress of loading is shown in splash screen animation.

`Menu` - state fired when everything is loaded and ready to go.  
Contains main menu and all its components.

`Select` - state fired when user clicked Cookie - game logo in main menu.  
Creates a list of user songs stored in `./static/Songs` directory.

`Game` - simply a game state, fired after choosing a song. **Work in progress!**

# Components
I am trying very hard to make this project by creating a standalone components,  
that can be just copy-pasted to other projects/games and work without many things to reconfigure.

List of working components:

`Audio` - works as an audio controller. Contains basic functions to manipulate HTML5 audio element,  
also calculates the average frequency in realtime, using [Web Audio API's AnalyserNode](https://developer.mozilla.org/pl/docs/Web/API/AnalyserNode).  
Located in `./src/Globals` directory.

`AudioManager` - communicates with `Audio`, but at first fetching beatmap audio data.  
Also used to navigate through beatmaps, getting random beatmap, etc.  
Located in `./src/Globals` directory.

`BeatmapParser` - heart of the project, used to parse beatmap data (files with .osu format) for later use.  
More info about _osu! file format_ in [official osu! wiki](https://osu.ppy.sh/help/wiki/osu!_File_Formats/Osu_(file_format).  
Located in `./src/Globals` directory.

`Alert` - creates an alert on the screen. It is just a simple transparent line with text.  
You can customize its duration, animation, colours and stuff.  
Located in `./src/Menu/objects` directory.

`MusicPlayer` - creates a music player - visual controls on screen.  
You can customize its position.  
Located in `./src/Menu/objects` directory.

More information about _components_ soon, probably in documentation not readme.

# Making a release

To package your app into an installer use command:

```
npm run release
```

It will start the packaging process and ready for distribution file will be outputted to `dist` directory.

All packaging actions are handled by [electron-builder](https://github.com/electron-userland/electron-builder). It has a lot of [customization options](https://github.com/electron-userland/electron-builder/wiki/Options), which you can declare under `build` key in your package.json.

# License

Released under the MIT license.
