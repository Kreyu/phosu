const ts = require('gulp-typescript')
const gulp = require('gulp')
const less = require('gulp-less')
const watch = require('gulp-watch')
const batch = require('gulp-batch')
const plumber = require('gulp-plumber')

const path = require('path')
const electron = require('electron')
const jetpack = require('fs-jetpack')
const rollup = require('rollup').rollup
const childProcess = require('child_process')
const argv = require('minimist')(process.argv)

const projectDir = jetpack
const srcDir = jetpack.cwd('./src')
const destDir = jetpack.cwd('./app')

const nodeBuiltInModules = ['assert', 'buffer', 'child_process', 'cluster',
  'console', 'constants', 'crypto', 'dgram', 'dns', 'domain', 'events',
  'fs', 'http', 'https', 'module', 'net', 'os', 'path', 'process', 'punycode',
  'querystring', 'readline', 'repl', 'stream', 'string_decoder', 'timers',
  'tls', 'tty', 'url', 'util', 'v8', 'vm', 'zlib'
]

const electronBuiltInModules = ['electron']

const generateExternalModulesList = () => {
    const appManifest = jetpack.read('./package.json', 'json')

    return [].concat(
        nodeBuiltInModules,
        electronBuiltInModules,
        Object.keys(appManifest.dependencies),
        Object.keys(appManifest.devDependencies)
    )
}

const cached = {}

gulp.task('bundle', () => {
    return Promise.all([
        bundle(srcDir.path('background.js'), destDir.path('background.js')),
        bundle(srcDir.path('app.js'), destDir.path('app.js')),
    ])
})

gulp.task('watch', () => {
    const beepOnError = (done) => {
        return (err) => {
            if (err) utils.beepSound()
            done(err)
        }
    }

    watch('src/**/*.js', batch((events, done) => 
        gulp.start('bundle', beepOnError(done))
    ))

    watch('src/**/*.less', batch((events, done) => 
        gulp.start('less', beepOnError(done))
    ))
})

gulp.task('build', ['bundle']);

gulp.task('start', ['build', 'watch'], () => {
    childProcess
        .spawn(electron, ['.'], { stdio: 'inherit' })
        .on('close', () => process.exit());
});

function bundle(src, dest, opts) {
    const options = opts || {};
    const plugins = [];

    return rollup({
        entry: src,
        external: generateExternalModulesList(),
        cache: cached[src],
        plugins: plugins.concat(options.rollupPlugins || []),
    }).then((bundle) => {
        cached[src] = bundle;

        const jsFile = path.basename(dest);

        return bundle.generate({
            format: 'cjs',
            sourceMap: true,
            sourceMapFile: jsFile,
        }).then(result => {
            const isolatedCode = `(function () {${result.code}\n}());`;
            return Promise.all([
            jetpack.writeAsync(dest, `${isolatedCode}\n//# sourceMappingURL=${jsFile}.map`),
            jetpack.writeAsync(`${dest}.map`, result.map.toString()),
            ]);
        })
    }).catch(e => {
        console.error(e);
        throw e;
    });
}

const utils = {
    getEnvName: () => argv.env || 'development',
    beepSound: () => process.stdout.write('\u0007')
}
